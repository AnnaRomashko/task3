#include "mainwindow.h"
#include "ui_mainwindow.h"

//#include "missionwork.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    hide_mission_menu();
    hide_change();
    hide_delete();
    hide_exchange();
    hide_mission_buttons();
    ui->labelMissionName->show();
    ui->textBrowser->show();

    ui->textBrowser->setText("No mission\nSelect action from menu");

    connect(ui->actionCreate_mission,&QAction::triggered,this,&MainWindow::create_selected);
    connect(ui->actionOpen_mission,&QAction::triggered,this,&MainWindow::open_selected);
    connect(ui->actionSave_mission,&QAction::triggered,this,&MainWindow::save_selected);
    connect(ui->actionClose_mission,&QAction::triggered,this,&MainWindow::close_selected);

    ActNow=actStart;
    ToDoNow=doStart;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::hide_mission_menu()
{
    ui->labelName->hide();
    ui->lineEditName->hide();
    ui->labelStart->hide();
    ui->labelStartX->hide();
    ui->lineEditStartX->hide();
    ui->labelStartY->hide();
    ui->lineEditStartY->hide();
    ui->labelStartZ->hide();
    ui->lineEditStartZ->hide();
    ui->labelStartDX->hide();
    ui->lineEditStartDX->hide();
    ui->labelStartDY->hide();
    ui->lineEditStartDY->hide();
    ui->labelStartDZ->hide();
    ui->lineEditStartDZ->hide();
    ui->labelEnd->hide();
    ui->labelEndX->hide();
    ui->lineEditEndX->hide();
    ui->labelEndY->hide();
    ui->lineEditEndY->hide();
    ui->labelEndZ->hide();
    ui->lineEditEndZ->hide();
    ui->labelEndDX->hide();
    ui->lineEditEndDX->hide();
    ui->labelEndDY->hide();
    ui->lineEditEndDY->hide();
    ui->labelEndDZ->hide();
    ui->lineEditEndDZ->hide();
    ui->labelDepth->hide();
    ui->labelDepthH->hide();
    ui->lineEditDepthH->hide();
    ui->labelDepthdH->hide();
    ui->lineEditDepthdH->hide();
    ui->labelPitch->hide();
    ui->lineEditPitch->hide();
    ui->labelLength->hide();
    ui->lineEditLength->hide();
    ui->labelHight->hide();
    ui->lineEditHight->hide();
    ui->labelSensor->hide();
    ui->comboBoxSensor->hide();
    ui->labelDive->hide();
    ui->comboBoxDive->hide();
}

void MainWindow::hide_mission_buttons()
{
    ui->pushButtonDive->hide();
    ui->pushButtonGals->hide();
    ui->pushButtonLift->hide();
    ui->pushButtonMove->hide();
    ui->pushButtonChange->hide();
    ui->pushButtonDelete->hide();
    ui->pushButtonExchange->hide();
    ui->pushButtonOk->hide();
    hide_change();
    hide_exchange();
    hide_delete();
}

void MainWindow::hide_change()
{
    ui->comboBoxChange->hide();
}

void MainWindow::hide_exchange()
{
    ui->comboBoxExchange1->hide();
    ui->comboBoxExchange2->hide();
}

void MainWindow::hide_delete()
{
    ui->comboBoxDelete->hide();
}

void MainWindow::show_mission_menu()
{
    ui->labelName->show();
    ui->lineEditName->show();
    ui->labelStart->show();
    ui->labelStartX->show();
    ui->lineEditStartX->show();
    ui->labelStartY->show();
    ui->lineEditStartY->show();
    ui->labelStartZ->show();
    ui->lineEditStartZ->show();
    ui->labelStartDX->show();
    ui->lineEditStartDX->show();
    ui->labelStartDY->show();
    ui->lineEditStartDY->show();
    ui->labelStartDZ->show();
    ui->lineEditStartDZ->show();
    ui->labelEnd->show();
    ui->labelEndX->show();
    ui->lineEditEndX->show();
    ui->labelEndY->show();
    ui->lineEditEndY->show();
    ui->labelEndZ->show();
    ui->lineEditEndZ->show();
    ui->labelEndDX->show();
    ui->lineEditEndDX->show();
    ui->labelEndDY->show();
    ui->lineEditEndDY->show();
    ui->labelEndDZ->show();
    ui->lineEditEndDZ->show();
    ui->labelDepth->show();
    ui->labelDepthH->show();
    ui->lineEditDepthH->show();
    ui->labelDepthdH->show();
    ui->lineEditDepthdH->show();
    ui->labelPitch->show();
    ui->lineEditPitch->show();
    ui->labelLength->show();
    ui->lineEditLength->show();
    ui->labelHight->show();
    ui->lineEditHight->show();
    ui->labelSensor->show();
    ui->comboBoxSensor->show();
    ui->labelDive->show();
    ui->comboBoxDive->show();
}

void MainWindow::show_mission_buttons()
{
    ui->pushButtonOk->show();
    ui->pushButtonDive->show();
    ui->pushButtonGals->show();
    ui->pushButtonLift->show();
    ui->pushButtonMove->show();
    ui->pushButtonChange->show();
    ui->pushButtonDelete->show();
    ui->pushButtonExchange->show();
}

void MainWindow::show_change(int count)
{
    ui->comboBoxChange->show();
    ui->comboBoxChange->clear();
    if (count<1) ui->comboBoxChange->addItem("0",0);
    else for (int i=1;i<=count;i++)
    {
        ui->comboBoxChange->addItem(QString::fromStdString(std::to_string(i)),i);
    }
}

void MainWindow::show_delete(int count)
{
    ui->comboBoxDelete->show();
    ui->comboBoxDelete->clear();
    if (count<1) ui->comboBoxDelete->addItem("0",0);
    else for (int i=1;i<=count;i++)
    {
        ui->comboBoxDelete->addItem(QString::fromStdString(std::to_string(i)),i);
    }
}

void MainWindow::show_exchange(int count)
{
    ui->comboBoxExchange1->show();
    ui->comboBoxExchange1->clear();
    if (count<1) ui->comboBoxExchange1->addItem("0",0);
    else for (int i=1;i<=count;i++)
    {
        ui->comboBoxExchange1->addItem(QString::fromStdString(std::to_string(i)),i);
    }
    ui->comboBoxExchange2->show();
    ui->comboBoxExchange2->clear();
    if (count<1) ui->comboBoxExchange2->addItem("0",0);
    else for (int i=1;i<=count;i++)
    {
        ui->comboBoxExchange2->addItem(QString::fromStdString(std::to_string(i)),i);
    }
}

void MainWindow::update_mission_show()
{
    ui->textBrowser->setText(QString::fromStdString(missionNow.print_mission_to_show()));
    MyReturn* retur = new MyReturn;
    if (missionNow.get_size() != 0)
    {
        retur->set_start_point(missionNow.get_end_point_mission(missionNow.get_size()-1));
        retur->set_end_point(missionNow.get_start_point_mission(0));
        missionNow.add_mission(retur);
        missionNow.print_mission_to_json("../Task3/MyJson.json",thisName);
        missionNow.print_mission_to_txt("../Task3/MyTxt.txt",thisName);
        missionNow.delete_mission(missionNow.get_size()-1);
    }
    else
    {
        missionNow.print_mission_to_json("../Task3/MyJson.json",thisName);
        missionNow.print_mission_to_txt("../Task3/MyTxt.txt",thisName);
    }
    delete retur;
}

void MainWindow::create_selected()
{
    ActNow=actCreate;
    hide_change();
    hide_delete();
    hide_exchange();
    hide_mission_menu();
    hide_mission_buttons();
    ui->labelName->show();
    ui->lineEditName->setText("Enter mission name");
    ui->lineEditName->show();
    ui->textBrowser->setText("No mission\n Press OK to create mission");
    ui->pushButtonOk->show();
}

void MainWindow::open_selected()
{
    ActNow=actWait;
    hide_change();
    hide_delete();
    hide_exchange();
    hide_mission_menu();
    show_mission_buttons();
    QString fileName = QFileDialog::getOpenFileName(this,"Select mission file","","*.json");
    thisName=this->scan_mission_from_json(fileName.toStdString());
    ui->labelMissionName->setText("Mission "+ QString::fromStdString(thisName));
    this->update_mission_show();
    ActNow=actModify;
}

void MainWindow::save_selected()
{
    ActNow=actWait;
    MyReturn* ret = new MyReturn;
    QString dir = QFileDialog::getExistingDirectory(this, "Select where to save mission",
                                                    "",
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);
    if (missionNow.get_size() != 0)
    {
        ret->set_start_point(missionNow.get_end_point_mission(missionNow.get_size()-1));
        ret->set_end_point(missionNow.get_start_point_mission(0));
        MyPoint p;
        p.X=precDefault;
        p.Y=precDefault;
        p.Z=precDefault;
        ret->set_precision_start(p);
        ret->set_precision_end(p);
        missionNow.add_mission(ret);
    }
    std::string fileName=dir.toStdString() + "/" + thisName + ".json";
    missionNow.print_mission_to_json(fileName,thisName);
    fileName=dir.toStdString() + "/" + thisName + ".txt";
    missionNow.print_mission_to_txt(fileName,thisName);
    ActNow=actModify;
    missionNow.delete_mission(missionNow.get_size()-1);
    delete ret;
}

void MainWindow::close_selected()
{
    ActNow=actWait;
    QMessageBox msgBox;
    msgBox.setText("Save mission");
    msgBox.setInformativeText("Do you want to save mission?");
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);
    int ret = msgBox.exec();
    switch(ret)
    {
    case QMessageBox::Save:
    {
        save_selected();
        ActNow=actModify;
        hide_change();
        hide_delete();
        hide_exchange();
        hide_mission_menu();
        hide_mission_buttons();
        ui->labelMissionName->setText("Mission");
        ui->textBrowser->setText("No mission\nSelect action from menu");
        break;
    }
    case QMessageBox::Discard:
    {
        ActNow=actModify;
        hide_change();
        hide_delete();
        hide_exchange();
        hide_mission_menu();
        hide_mission_buttons();
        ui->labelMissionName->setText("Mission");
        ui->textBrowser->setText("No mission\nSelect action from menu");
        break;
    }
    case QMessageBox::Cancel:
    {
        ActNow=actModify;
        break;
    }
    default:
    {
        ActNow=actModify;
        break;
    }
    }
}

void MainWindow::on_pushButtonDive_clicked()
{
    ToDoNow=doDive;
    show_mission_menu();
    hide_change();
    hide_exchange();
    hide_delete();
    ui->lineEditName->setText("Dive");
    if (missionNow.get_size() == 0)
    {
        ui->lineEditStartX->setValue(0);
        ui->lineEditStartY->setValue(0);
        ui->lineEditStartY->setReadOnly(true);
        ui->lineEditStartZ->setValue(0);
    }
    else
    {
        MyPoint start=missionNow.get_end_point_mission(missionNow.get_size()-1);
        ui->lineEditStartX->setValue(start.X);
        ui->lineEditStartX->setReadOnly(true);
        ui->lineEditStartY->setValue(start.Y);
        ui->lineEditStartY->setReadOnly(true);
        ui->lineEditStartZ->setValue(start.Z);
        ui->lineEditStartZ->setReadOnly(true);
    }
    ui->lineEditStartDX->setValue(precDefault);
    ui->lineEditStartDY->setValue(precDefault);
    ui->lineEditStartDZ->setValue(precDefault);
    ui->lineEditEndX->setValue(0);
    ui->lineEditEndY->setValue(0);
    ui->lineEditEndZ->setValue(0);
    ui->lineEditEndDX->setValue(precDefault);
    ui->lineEditEndDY->setValue(precDefault);
    ui->lineEditEndDZ->setValue(precDefault);
    ui->lineEditDepthH->setValue(0);
    ui->lineEditDepthdH->setValue(precDefault);
    ui->labelLength->setText("Radius");
    ui->lineEditLength->setValue(0);
    ui->lineEditPitch->setValue(0);
    ui->labelHight->hide();
    ui->lineEditHight->hide();
    ui->labelSensor->setText("Sensor type");
    ui->comboBoxSensor->clear();
    ui->comboBoxSensor->addItem("Depth sensor",depthSensor);
    ui->comboBoxSensor->addItem("Sonar", sonarSensor);
    ui->labelDive->setText("Dive type");
    ui->comboBoxDive->clear();
    ui->comboBoxDive->addItem("Spiral",spiralDive);
    ui->comboBoxDive->addItem("Vertical",verticalDive);
    ui->comboBoxDive->addItem("Line",lineDive);
}

void MainWindow::on_pushButtonLift_clicked()
{
    ToDoNow=doLift;
    show_mission_menu();
    hide_change();
    hide_exchange();
    hide_delete();
    ui->lineEditName->setText("Lift");
    if (missionNow.get_size() == 0)
    {
        hide_mission_menu();
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Lift cant be first operation");
        msgBox.exec();
        return;
    }
    else
    {
        MyPoint start=missionNow.get_end_point_mission(missionNow.get_size()-1);
        ui->lineEditStartX->setValue(start.X);
        ui->lineEditStartX->setReadOnly(true);
        ui->lineEditStartY->setValue(start.Y);
        ui->lineEditStartY->setReadOnly(true);
        ui->lineEditStartZ->setValue(start.Z);
        ui->lineEditStartZ->setReadOnly(true);
    }
    ui->lineEditStartDX->setValue(precDefault);
    ui->lineEditStartDY->setValue(precDefault);
    ui->lineEditStartDZ->setValue(precDefault);
    ui->lineEditEndX->setValue(0);
    ui->lineEditEndY->setValue(0);
    ui->lineEditEndZ->setValue(0);
    ui->lineEditEndDX->setValue(precDefault);
    ui->lineEditEndDY->setValue(precDefault);
    ui->lineEditEndDZ->setValue(precDefault);
    ui->lineEditDepthH->setValue(0);
    ui->lineEditDepthdH->setValue(precDefault);
    ui->labelLength->setText("Radius");
    ui->lineEditLength->setValue(0);
    ui->lineEditPitch->setValue(0);
    ui->labelHight->hide();
    ui->lineEditHight->hide();
    ui->labelSensor->setText("Sensor type");
    ui->comboBoxSensor->clear();
    ui->comboBoxSensor->addItem("Depth sensor",depthSensor);
    ui->comboBoxSensor->addItem("Sonar", sonarSensor);
    ui->labelDive->setText("Lift type");
    ui->comboBoxDive->clear();
    ui->comboBoxDive->addItem("Spiral",spiralDive);
    ui->comboBoxDive->addItem("Vertical",verticalDive);
    ui->comboBoxDive->addItem("Line",lineDive);
}


void MainWindow::on_pushButtonMove_clicked()
{
    ToDoNow=doMove;
    show_mission_menu();
    hide_change();
    hide_exchange();
    hide_delete();
    ui->lineEditName->setText("Move");
    if (missionNow.get_size() == 0)
    {
        ui->lineEditStartX->setValue(0);
        ui->lineEditStartY->setValue(0);
        ui->lineEditStartY->setReadOnly(true);
        ui->lineEditStartZ->setValue(0);
    }
    else
    {
        MyPoint start=missionNow.get_end_point_mission(missionNow.get_size()-1);
        ui->lineEditStartX->setValue(start.X);
        ui->lineEditStartX->setReadOnly(true);
        ui->lineEditStartY->setValue(start.Y);
        ui->lineEditStartY->setReadOnly(true);
        ui->lineEditStartZ->setValue(start.Z);
        ui->lineEditStartZ->setReadOnly(true);
    }
    ui->lineEditStartDX->setValue(precDefault);
    ui->lineEditStartDY->setValue(precDefault);
    ui->lineEditStartDZ->setValue(precDefault);
    ui->lineEditEndX->setValue(0);
    ui->lineEditEndY->setValue(0);
    ui->lineEditEndZ->setValue(0);
    ui->lineEditEndDX->setValue(precDefault);
    ui->lineEditEndDY->setValue(precDefault);
    ui->lineEditEndDZ->setValue(precDefault);
    ui->labelDepth->hide();
    ui->labelDepthH->hide();
    ui->lineEditDepthH->hide();
    ui->labelDepthdH->hide();
    ui->lineEditDepthdH->hide();
    ui->labelPitch->hide();
    ui->lineEditPitch->hide();
    ui->labelLength->hide();
    ui->lineEditLength->hide();
    ui->labelHight->hide();
    ui->lineEditHight->hide();
    ui->labelSensor->setText("Move type");
    ui->comboBoxSensor->clear();
    ui->comboBoxSensor->addItem("Move to point",pointMove);
    ui->comboBoxSensor->addItem("Line move", lineMove);
    ui->labelDive->setText("Depth type");
    ui->comboBoxDive->clear();
    ui->comboBoxDive->addItem("Constant depth",constDepth);
    ui->comboBoxDive->addItem("Constant hight",constHight);
    ui->comboBoxDive->addItem("Any depth",anyDepth);
}

void MainWindow::on_pushButtonGals_clicked()
{
    ToDoNow=doGals;
    show_mission_menu();
    hide_change();
    hide_exchange();
    hide_delete();
    ui->lineEditName->setText("Gals");
    if (missionNow.get_size() == 0)
    {
        ui->lineEditStartX->setValue(0);
        ui->lineEditStartY->setValue(0);
        ui->lineEditStartY->setReadOnly(true);
        ui->lineEditStartZ->setValue(0);
    }
    else
    {
        MyPoint start=missionNow.get_end_point_mission(missionNow.get_size()-1);
        ui->lineEditStartX->setValue(start.X);
        ui->lineEditStartX->setReadOnly(true);
        ui->lineEditStartY->setValue(start.Y);
        ui->lineEditStartY->setReadOnly(true);
        ui->lineEditStartZ->setValue(start.Z);
        ui->lineEditStartZ->setReadOnly(true);
    }
    ui->lineEditStartDX->setValue(precDefault);
    ui->lineEditStartDY->setValue(precDefault);
    ui->lineEditStartDZ->setValue(precDefault);
    ui->lineEditEndX->setValue(0);
    ui->lineEditEndY->setValue(0);
    ui->lineEditEndZ->setValue(0);
    ui->lineEditEndDX->setValue(precDefault);
    ui->lineEditEndDY->setValue(precDefault);
    ui->lineEditEndDZ->setValue(precDefault);
    ui->lineEditDepthH->hide();
    ui->lineEditDepthdH->hide();
    ui->labelLength->setText("Length");
    ui->lineEditLength->setValue(0);
    ui->lineEditPitch->setValue(0);
    ui->lineEditHight->setValue(0);
    ui->labelSensor->hide();
    ui->comboBoxSensor->hide();
    ui->labelDive->hide();
    ui->comboBoxDive->hide();
}

void MainWindow::on_pushButtonChange_clicked()
{
    ToDoNow=doChangeStart;
    show_change(missionNow.get_size());
    hide_exchange();
    hide_delete();
    hide_mission_menu();
}

void MainWindow::on_pushButtonExchange_clicked()
{
    ToDoNow=doExchange;
    show_exchange(missionNow.get_size());
    hide_change();
    hide_delete();
    hide_mission_menu();
}

void MainWindow::on_pushButtonDelete_clicked()
{
    ToDoNow=doDelete;
    show_delete(missionNow.get_size());
    hide_change();
    hide_exchange();
    hide_mission_menu();
}

void MainWindow::on_pushButtonOk_clicked()
{
    switch(ActNow)
    {
    case actStart:
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Something wrong with button logic in actStart");
        msgBox.exec();
        break;
    }
    case actCreate:
    {
        thisName=ui->lineEditName->text().toStdString();
        ui->labelMissionName->setText("Mission " + ui->lineEditName->text());
        ui->textBrowser->setText("Empty mission");
        show_mission_buttons();
        missionNow.clear_mission();
        ui->labelName->hide();
        ui->lineEditName->hide();
        ActNow=actModify;
        break;
    }
    case actWait:
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Something wrong with button logic in actWait");
        msgBox.exec();
        break;
    }
    case actModify:
    {
        switch (ToDoNow)
        {
        case doStart:
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Error");
            msgBox.setText("Select action and press OK");
            msgBox.exec();
            break;
        }
        case doDive:
        {
            dive_selected();
            break;
        }
        case doLift:
        {
            lift_selected();
            break;
        }
        case doMove:
        {
            move_selected();
            break;
        }
        case doGals:
        {
            gals_selected();
            break;
        }
        case doChangeStart:
        {
            show_selected_mission();
            break;
        }
        case doChangeEnd:
        {
            change_selected();
            break;
        }
        case doExchange:
        {
            exchange_selected();
            break;
        }
        case doDelete:
        {
            delete_selected();
            break;
        }
        }
    }
    }
}

void MainWindow::dive_selected()
{
    MyDive* dive=new MyDive;
    dive->set_name(ui->lineEditName->text().toStdString());
    MyPoint p;
    p.X=ui->lineEditStartX->value();
    p.Y=ui->lineEditStartY->value();
    p.Z=ui->lineEditStartZ->value();
    dive->set_start_point(p);
    if (dive->get_error() != "OK")
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText(QString::fromStdString(dive->get_error()));
        msgBox.exec();
        return;
    }
    p.X=ui->lineEditStartDX->value();
    p.Y=ui->lineEditStartDY->value();
    p.Z=ui->lineEditStartDZ->value();
    dive->set_precision_start(p);
    if (dive->get_error() != "OK")
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText(QString::fromStdString(dive->get_error()));
        msgBox.exec();
        return;
    }
    if (ui->comboBoxSensor->currentIndex() == 0) dive->set_sensor_type(depthSensor);
    else if (ui->comboBoxSensor->currentIndex() == 1) dive->set_sensor_type(sonarSensor);
    if (ui->comboBoxDive->currentIndex() == 0) dive->set_dive_type(spiralDive);
    else if (ui->comboBoxDive->currentIndex() == 1) dive->set_dive_type(verticalDive);
    else if (ui->comboBoxDive->currentIndex() == 2) dive->set_dive_type(lineDive);
    if (dive->get_dive_type() == verticalDive)
    {
        double h=ui->lineEditDepthH->value();
        dive->set_depth(h);
        if (dive->get_error() != "OK")
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Error");
            msgBox.setText(QString::fromStdString(dive->get_error()));
            msgBox.exec();
            return;
        }
        h=ui->lineEditDepthdH->value();
        dive->set_depth_precision(h);
        if (dive->get_error() != "OK")
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Error");
            msgBox.setText(QString::fromStdString(dive->get_error()));
            msgBox.exec();
            return;
        }
    }
    else
    {
        p.X=ui->lineEditEndX->value();
        p.Y=ui->lineEditEndY->value();
        p.Z=ui->lineEditEndZ->value();
        dive->set_end_point(p);
        if (dive->get_error() != "OK")
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Error");
            msgBox.setText(QString::fromStdString(dive->get_error()));
            msgBox.exec();
            return;
        }
        p.X=ui->lineEditEndDX->value();
        p.Y=ui->lineEditEndDY->value();
        p.Z=ui->lineEditEndDZ->value();
        dive->set_precision_end(p);
        if (dive->get_error() != "OK")
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Error");
            msgBox.setText(QString::fromStdString(dive->get_error()));
            msgBox.exec();
            return;
        }
    }
    if (dive->get_dive_type() == spiralDive)
    {
        double r=ui->lineEditLength->value();
        double p=ui->lineEditPitch->value();
        Spiral s(dive->get_start_point(),dive->get_end_point(),r,p);
        if (s.errorSpiral != "OK")
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Error");
            msgBox.setText(QString::fromStdString(s.errorSpiral));
            msgBox.exec();
            return;
        }
        dive->set_spiral_param(s);
        if (dive->get_error() != "OK")
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Error");
            msgBox.setText(QString::fromStdString(dive->get_error()));
            msgBox.exec();
            return;
        }
    }
    missionNow.add_mission(dive);
    update_mission_show();
    ToDoNow=doStart;
    hide_mission_menu();
}

void MainWindow::lift_selected()
{
    MyLift* lift = new MyLift;
    lift->set_name(ui->lineEditName->text().toStdString());
    MyPoint p;
    p.X=ui->lineEditStartX->value();
    p.Y=ui->lineEditStartY->value();
    p.Z=ui->lineEditStartZ->value();
    lift->set_start_point(p);
    if (lift->get_error() != "OK")
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText(QString::fromStdString(lift->get_error()));
        msgBox.exec();
        return;
    }
    p.X=ui->lineEditStartDX->value();
    p.Y=ui->lineEditStartDY->value();
    p.Z=ui->lineEditStartDZ->value();
    lift->set_precision_start(p);
    if (lift->get_error() != "OK")
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText(QString::fromStdString(lift->get_error()));
        msgBox.exec();
        return;
    }
    if (ui->comboBoxSensor->currentIndex() == 0) lift->set_sensor_type(depthSensor);
    else if (ui->comboBoxSensor->currentIndex() == 1) lift->set_sensor_type(sonarSensor);
    if (ui->comboBoxDive->currentIndex() == 0) lift->set_dive_type(spiralDive);
    else if (ui->comboBoxDive->currentIndex() == 1) lift->set_dive_type(verticalDive);
    else if (ui->comboBoxDive->currentIndex() == 2) lift->set_dive_type(lineDive);
    if (lift->get_dive_type() == verticalDive)
    {
        double h=ui->lineEditDepthH->value();
        lift->set_depth(h);
        if (lift->get_error() != "OK")
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Error");
            msgBox.setText(QString::fromStdString(lift->get_error()));
            msgBox.exec();
            return;
        }
        h=ui->lineEditDepthdH->value();
        lift->set_depth_precision(h);
        if (lift->get_error() != "OK")
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Error");
            msgBox.setText(QString::fromStdString(lift->get_error()));
            msgBox.exec();
            return;
        }
    }
    else
    {
        p.X=ui->lineEditEndX->value();
        p.Y=ui->lineEditEndY->value();
        p.Z=ui->lineEditEndZ->value();
        lift->set_end_point(p);
        if (lift->get_error() != "OK")
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Error");
            msgBox.setText(QString::fromStdString(lift->get_error()));
            msgBox.exec();
            return;
        }
        p.X=ui->lineEditEndDX->value();
        p.Y=ui->lineEditEndDY->value();
        p.Z=ui->lineEditEndDZ->value();
        lift->set_precision_end(p);
        if (lift->get_error() != "OK")
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Error");
            msgBox.setText(QString::fromStdString(lift->get_error()));
            msgBox.exec();
            return;
        }
    }
    if (lift->get_dive_type() == spiralDive)
    {
        double r=ui->lineEditLength->value();
        double p=ui->lineEditPitch->value();
        Spiral s(lift->get_start_point(),lift->get_end_point(),r,p);
        if (s.errorSpiral != "OK")
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Error");
            msgBox.setText(QString::fromStdString(s.errorSpiral));
            msgBox.exec();
            return;
        }
        lift->set_spiral_param(s);
        if (lift->get_error() != "OK")
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Error");
            msgBox.setText(QString::fromStdString(lift->get_error()));
            msgBox.exec();
            return;
        }
    }
    missionNow.add_mission(lift);
    update_mission_show();
    ToDoNow=doStart;
    hide_mission_menu();
}

void MainWindow::move_selected()
{
    MyMove* move = new MyMove;
    move->set_name(ui->lineEditName->text().toStdString());
    MyPoint p;
    p.X=ui->lineEditStartX->value();
    p.Y=ui->lineEditStartY->value();
    p.Z=ui->lineEditStartZ->value();
    move->set_start_point(p);
    if (move->get_error() != "OK")
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText(QString::fromStdString(move->get_error()));
        msgBox.exec();
        return;
    }
    p.X=ui->lineEditStartDX->value();
    p.Y=ui->lineEditStartDY->value();
    p.Z=ui->lineEditStartDZ->value();
    move->set_precision_start(p);
    if (move->get_error() != "OK")
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText(QString::fromStdString(move->get_error()));
        msgBox.exec();
        return;
    }
    if (ui->comboBoxSensor->currentIndex() == 0) move->set_move_type(pointMove);
    else if (ui->comboBoxSensor->currentIndex() == 1) move->set_move_type(lineMove);
    if (ui->comboBoxDive->currentIndex() == 0) move->set_depth_type(constDepth);
    else if (ui->comboBoxDive->currentIndex() == 1) move->set_depth_type(constHight);
    else if (ui->comboBoxDive->currentIndex() == 2) move->set_depth_type(anyDepth);
    if (move->get_depth_type() == anyDepth)
    {
        p.X=ui->lineEditEndX->value();
        p.Y=ui->lineEditEndY->value();
        p.Z=ui->lineEditEndZ->value();
        move->set_end_point(p);
        if (move->get_error() != "OK")
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Error");
            msgBox.setText(QString::fromStdString(move->get_error()));
            msgBox.exec();
            return;
        }
    }
    else
    {
        p.X=ui->lineEditEndX->value();
        p.Y=ui->lineEditStartY->value();
        p.Z=ui->lineEditEndZ->value();
        move->set_end_point(p);
        if (move->get_error() != "OK")
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Error");
            msgBox.setText(QString::fromStdString(move->get_error()));
            msgBox.exec();
            return;
        }
    }
    p.X=ui->lineEditEndDX->value();
    p.Y=ui->lineEditEndDY->value();
    p.Z=ui->lineEditEndDZ->value();
    move->set_precision_end(p);
    if (move->get_error() != "OK")
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText(QString::fromStdString(move->get_error()));
        msgBox.exec();
        return;
    }
    missionNow.add_mission(move);
    update_mission_show();
    ToDoNow=doStart;
    hide_mission_menu();
}

void MainWindow::gals_selected()
{
    MyGals* gals = new MyGals;
    gals->set_name(ui->lineEditName->text().toStdString());
    MyPoint p;
    p.X=ui->lineEditStartX->value();
    p.Y=ui->lineEditStartY->value();
    p.Z=ui->lineEditStartZ->value();
    gals->set_start_point(p);
    if (gals->get_error() != "OK")
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText(QString::fromStdString(gals->get_error()));
        msgBox.exec();
        return;
    }
    p.X=ui->lineEditStartDX->value();
    p.Y=ui->lineEditStartDY->value();
    p.Z=ui->lineEditStartDZ->value();
    gals->set_precision_start(p);
    if (gals->get_error() != "OK")
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText(QString::fromStdString(gals->get_error()));
        msgBox.exec();
        return;
    }
    p.X=ui->lineEditEndX->value();
    p.Y=ui->lineEditStartY->value();
    p.Z=ui->lineEditEndZ->value();
    gals->set_end_point(p);
    if (gals->get_error() != "OK")
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText(QString::fromStdString(gals->get_error()));
        msgBox.exec();
        return;
    }
    p.X=ui->lineEditEndDX->value();
    p.Y=ui->lineEditEndDY->value();
    p.Z=ui->lineEditEndDZ->value();
    gals->set_precision_end(p);
    if (gals->get_error() != "OK")
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText(QString::fromStdString(gals->get_error()));
        msgBox.exec();
        return;
    }
    double h=ui->lineEditPitch->value();
    gals->set_pitch(h);
    if (gals->get_error() != "OK")
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText(QString::fromStdString(gals->get_error()));
        msgBox.exec();
        return;
    }
    h=ui->lineEditLength->value();
    gals->set_length();
    if (gals->get_error() != "OK")
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText(QString::fromStdString(gals->get_error()));
        msgBox.exec();
        return;
    }
    h=ui->lineEditHight->value();
    gals->set_hight(h);
    if (gals->get_error() != "OK")
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText(QString::fromStdString(gals->get_error()));
        msgBox.exec();
        return;
    }
    missionNow.add_mission(gals);
    update_mission_show();
    ToDoNow=doStart;
    hide_mission_menu();
}

void MainWindow::show_selected_mission()
{
    QString sn=ui->comboBoxChange->currentText();
    if (sn == "0")
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Mission is empty. Nothing to change.");
        msgBox.exec();
        hide_change();
        ToDoNow=doStart;
        return;
    }
    int nn=ui->comboBoxChange->currentIndex();
    MyDive* dive = nullptr;
    MyLift* lift = nullptr;
    MyMove* move = nullptr;
    MyGals* gals = nullptr;
    switch(missionNow.get_mission_type(missionNow.get_mission(nn)))
    {
    case typeDive:
    {
        dive=dynamic_cast<MyDive*>(missionNow.get_mission(nn));
        on_pushButtonDive_clicked();
        currentMissionType=doDive;
        ToDoNow=doChangeStart;
        ui->lineEditName->setText(QString::fromStdString(dive->get_name()));
        MyPoint p=dive->get_start_point();
        ui->lineEditStartX->setValue(p.X);
        ui->lineEditStartY->setValue(p.Y);
        ui->lineEditStartZ->setValue(p.Z);
        p=dive->get_precision_start();
        ui->lineEditStartDX->setValue(p.X);
        ui->lineEditStartDY->setValue(p.Y);
        ui->lineEditStartDZ->setValue(p.Z);
        p=dive->get_end_point();
        ui->lineEditEndX->setValue(p.X);
        ui->lineEditEndY->setValue(p.Y);
        ui->lineEditEndZ->setValue(p.Z);
        p=dive->get_precision_end();
        ui->lineEditEndDX->setValue(p.X);
        ui->lineEditEndDY->setValue(p.Y);
        ui->lineEditEndDZ->setValue(p.Z);
        ui->lineEditDepthH->setValue(dive->get_depth());
        ui->lineEditDepthdH->setValue(dive->get_depth_precision());
        ui->comboBoxDive->setCurrentIndex(dive->get_dive_type());
        ui->comboBoxSensor->setCurrentIndex(dive->get_sensor_type());
        Spiral s=dive->get_spiral_param();
        ui->lineEditPitch->setValue(s.pitchSpiral);
        ui->lineEditLength->setValue(s.radiusSpiral);
        break;
    }
    case typeLift:
    {
        lift=dynamic_cast<MyLift*>(missionNow.get_mission(nn));
        on_pushButtonLift_clicked();
        currentMissionType=doLift;
        ToDoNow=doChangeStart;
        ui->lineEditName->setText(QString::fromStdString(lift->get_name()));
        MyPoint p=lift->get_start_point();
        ui->lineEditStartX->setValue(p.X);
        ui->lineEditStartY->setValue(p.Y);
        ui->lineEditStartZ->setValue(p.Z);
        p=lift->get_precision_start();
        ui->lineEditStartDX->setValue(p.X);
        ui->lineEditStartDY->setValue(p.Y);
        ui->lineEditStartDZ->setValue(p.Z);
        p=lift->get_end_point();
        ui->lineEditEndX->setValue(p.X);
        ui->lineEditEndY->setValue(p.Y);
        ui->lineEditEndZ->setValue(p.Z);
        p=lift->get_precision_end();
        ui->lineEditEndDX->setValue(p.X);
        ui->lineEditEndDY->setValue(p.Y);
        ui->lineEditEndDZ->setValue(p.Z);
        ui->lineEditDepthH->setValue(lift->get_depth());
        ui->lineEditDepthdH->setValue(lift->get_depth_precision());
        ui->comboBoxDive->setCurrentIndex(lift->get_dive_type());
        ui->comboBoxSensor->setCurrentIndex(lift->get_sensor_type());
        Spiral s=lift->get_spiral_param();
        ui->lineEditPitch->setValue(s.pitchSpiral);
        ui->lineEditLength->setValue(s.radiusSpiral);
        break;
    }
    case typeMove:
    {
        move=dynamic_cast<MyMove*>(missionNow.get_mission(nn));
        on_pushButtonMove_clicked();
        currentMissionType=doMove;
        ToDoNow=doChangeStart;
        ui->lineEditName->setText(QString::fromStdString(move->get_name()));
        MyPoint p=move->get_start_point();
        ui->lineEditStartX->setValue(p.X);
        ui->lineEditStartY->setValue(p.Y);
        ui->lineEditStartZ->setValue(p.Z);
        p=move->get_precision_start();
        ui->lineEditStartDX->setValue(p.X);
        ui->lineEditStartDY->setValue(p.Y);
        ui->lineEditStartDZ->setValue(p.Z);
        p=move->get_end_point();
        ui->lineEditEndX->setValue(p.X);
        ui->lineEditEndY->setValue(p.Y);
        ui->lineEditEndZ->setValue(p.Z);
        p=move->get_precision_end();
        ui->lineEditEndDX->setValue(p.X);
        ui->lineEditEndDY->setValue(p.Y);
        ui->lineEditEndDZ->setValue(p.Z);
        ui->comboBoxDive->setCurrentIndex(move->get_move_type());
        ui->comboBoxSensor->setCurrentIndex(move->get_depth_type());
        break;
    }
    case typeGals:
    {
        gals=dynamic_cast<MyGals*>(missionNow.get_mission(nn));
        on_pushButtonGals_clicked();
        currentMissionType=doGals;
        ToDoNow=doChangeStart;
        ui->lineEditName->setText(QString::fromStdString(gals->get_name()));
        MyPoint p=gals->get_start_point();
        ui->lineEditStartX->setValue(p.X);
        ui->lineEditStartY->setValue(p.Y);
        ui->lineEditStartZ->setValue(p.Z);
        p=gals->get_precision_start();
        ui->lineEditStartDX->setValue(p.X);
        ui->lineEditStartDY->setValue(p.Y);
        ui->lineEditStartDZ->setValue(p.Z);
        p=gals->get_end_point();
        ui->lineEditEndX->setValue(p.X);
        ui->lineEditEndY->setValue(p.Y);
        ui->lineEditEndZ->setValue(p.Z);
        p=gals->get_precision_end();
        ui->lineEditEndDX->setValue(p.X);
        ui->lineEditEndDY->setValue(p.Y);
        ui->lineEditEndDZ->setValue(p.Z);
        ui->lineEditPitch->setValue(gals->get_pitch());
        ui->lineEditLength->setValue(gals->get_length());
        ui->lineEditHight->setValue(gals->get_hight());
        break;
    }
    default:
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Something wrong with mission type");
        msgBox.exec();
        break;
    }
    }
    hide_change();
    ToDoNow=doChangeEnd;
}

void MainWindow::change_selected()
{
    switch(currentMissionType)
    {
    case doDive:
    {
        dive_selected();
        missionNow.exchange_mission(ui->comboBoxChange->currentIndex(),missionNow.get_size()-1);
        missionNow.delete_mission(missionNow.get_size()-1);
        update_mission_show();
        ToDoNow=doStart;
        hide_mission_menu();
        break;
    }
    case doLift:
    {
        lift_selected();
        missionNow.exchange_mission(ui->comboBoxChange->currentIndex(),missionNow.get_size()-1);
        missionNow.delete_mission(missionNow.get_size()-1);
        update_mission_show();
        ToDoNow=doStart;
        hide_mission_menu();
        break;
    }
    case doMove:
    {
        move_selected();
        missionNow.exchange_mission(ui->comboBoxChange->currentIndex(),missionNow.get_size()-1);
        missionNow.delete_mission(missionNow.get_size()-1);
        update_mission_show();
        ToDoNow=doStart;
        hide_mission_menu();
        break;
    }
    case doGals:
    {
        gals_selected();
        missionNow.exchange_mission(ui->comboBoxChange->currentIndex(),missionNow.get_size()-1);
        missionNow.delete_mission(missionNow.get_size()-1);
        update_mission_show();
        ToDoNow=doStart;
        hide_mission_menu();
        break;
    }
    default:
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Something wrong with mission type in currentMissionType");
        msgBox.exec();
        break;
    }
    }
}

void MainWindow::exchange_selected()
{
    ToDoNow=doStart;
    if (ui->comboBoxExchange1->currentIndex() == ui->comboBoxExchange2->currentIndex())
    {
        hide_exchange();
        return;
    }
    missionNow.exchange_mission(ui->comboBoxExchange1->currentIndex(), ui->comboBoxExchange2->currentIndex());
    update_mission_show();
    hide_exchange();
}

void MainWindow::delete_selected()
{
    ToDoNow=doStart;
    if (ui->comboBoxDelete->currentText() == "0")
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Mission is empty");
        msgBox.exec();
        hide_delete();
        return;
    }
    missionNow.delete_mission(ui->comboBoxDelete->currentIndex());
    update_mission_show();
    if (missionNow.get_size() == 0) ui->textBrowser->setText("Empty mission");
    hide_delete();
}

string MainWindow::scan_mission_from_json(string jsonName)
{
    QFile jsonFile(QString::fromStdString(jsonName));
    jsonFile.open(QIODevice::ReadOnly);
    QJsonDocument doc(QJsonDocument::fromJson(jsonFile.readAll()));
    QJsonObject mainObject=doc.object();
    QJsonValue val=mainObject["Mission"];
    string name=val.toString().toStdString();
    val=mainObject["Number of operations"];
    int n=val.toInt();
    missionNow.clear_mission();
    QJsonObject mission;
    for (int i=1;i<n;i++)
    {
        val=mainObject[QString::fromStdString("№ "+to_string(i))];
        mission=val.toObject();
        val=mission["Type"];
        string typeStr=val.toString().toStdString();
        if (typeStr == "Dive")
        {
            missionNow.add_mission(this->scan_dive_from_json(mission));
        }
        else if (typeStr == "Lift")
        {
            missionNow.add_mission(scan_lift_from_json(mission));
        }
        else if (typeStr == "Move")
        {
            missionNow.add_mission(scan_move_from_json(mission));
        }
        else if (typeStr == "Gals")
        {
            missionNow.add_mission(scan_gals_from_json(mission));
        }
    }
    jsonFile.close();
    return name;
}

MyDive *MainWindow::scan_dive_from_json(QJsonObject object)
{
    MyDive* dive= new MyDive;
    QJsonValue val;
    QJsonObject arr;
    MyPoint point;
    dive->set_name(object["Name"].toString().toStdString());
    val=object["Start Point"];
    arr=val.toObject();
    point.X=arr["X"].toDouble();
    point.Y=arr["Y"].toDouble();
    point.Z=arr["Z"].toDouble();
    dive->set_start_point(point);
    point.X=arr["dX"].toDouble();
    point.Y=arr["dY"].toDouble();
    point.Z=arr["dZ"].toDouble();
    dive->set_precision_start(point);
    val=object["End Point"];
    arr=val.toObject();
    point.X=arr["X"].toDouble();
    point.Y=arr["Y"].toDouble();
    point.Z=arr["Z"].toDouble();
    dive->set_end_point(point);
    point.X=arr["dX"].toDouble();
    point.Y=arr["dY"].toDouble();
    point.Z=arr["dZ"].toDouble();
    dive->set_precision_end(point);
    val=object["Depth"];
    arr=val.toObject();
    dive->calculate_depth();
    dive->set_depth_precision(arr["dH"].toDouble());
    string str=object["Sensor type"].toString().toStdString();
    if (str == "depth sensor")
    {
        dive->set_sensor_type(depthSensor);
    }
    else if (str == "sonar")
    {
        dive->set_sensor_type(sonarSensor);
    }
    str=object["Dive type"].toString().toStdString();
    if (str == "spiral")
    {
        dive->set_dive_type(spiralDive);
        double r=object["Spiral radius"].toDouble();
        double p=object["Spiral pitch"].toDouble();
        Spiral sp(dive->get_start_point(),dive->get_end_point(),r,p);
        dive->set_spiral_param(sp);
    }
    else if (str == "line")
    {
        dive->set_dive_type(lineDive);
    }
    else if (str == "vertical")
    {
        dive->set_dive_type(verticalDive);
    }
    return dive;
}

MyLift *MainWindow::scan_lift_from_json(QJsonObject object)
{
    MyLift* lift = new MyLift;
    QJsonValue val;
    QJsonObject arr;
    MyPoint point;
    lift->set_name(object["Name"].toString().toStdString());
    val=object["Start Point"];
    arr=val.toObject();
    point.X=arr["X"].toDouble();
    point.Y=arr["Y"].toDouble();
    point.Z=arr["Z"].toDouble();
    lift->set_start_point(point);
    point.X=arr["dX"].toDouble();
    point.Y=arr["dY"].toDouble();
    point.Z=arr["dZ"].toDouble();
    lift->set_precision_start(point);
    val=object["End Point"];
    arr=val.toObject();
    point.X=arr["X"].toDouble();
    point.Y=arr["Y"].toDouble();
    point.Z=arr["Z"].toDouble();
    lift->set_end_point(point);
    point.X=arr["dX"].toDouble();
    point.Y=arr["dY"].toDouble();
    point.Z=arr["dZ"].toDouble();
    lift->set_precision_end(point);
    val=object["Depth"];
    arr=val.toObject();
    lift->calculate_depth();
    lift->set_depth_precision(arr["dH"].toDouble());
    string str=object["Sensor type"].toString().toStdString();
    if (str == "depth sensor")
    {
        lift->set_sensor_type(depthSensor);
    }
    else if (str == "sonar")
    {
        lift->set_sensor_type(sonarSensor);
    }
    str=object["Dive type"].toString().toStdString();
    if (str == "spiral")
    {
        lift->set_dive_type(spiralDive);
        double r=object["Spiral radius"].toDouble();
        double p=object["Spiral pitch"].toDouble();
        Spiral sp(lift->get_start_point(),lift->get_end_point(),r,p);
        lift->set_spiral_param(sp);
    }
    else if (str == "line")
    {
        lift->set_dive_type(lineDive);
    }
    else if (str == "vertical")
    {
        lift->set_dive_type(verticalDive);
    }
    return lift;
}

MyMove *MainWindow::scan_move_from_json(QJsonObject object)
{
    MyMove* move = new MyMove;
    QJsonValue val;
    QJsonObject arr;
    MyPoint point;
    move->set_name(object["Name"].toString().toStdString());
    val=object["Start Point"];
    arr=val.toObject();
    point.X=arr["X"].toDouble();
    point.Y=arr["Y"].toDouble();
    point.Z=arr["Z"].toDouble();
    move->set_start_point(point);
    point.X=arr["dX"].toDouble();
    point.Y=arr["dY"].toDouble();
    point.Z=arr["dZ"].toDouble();
    move->set_precision_start(point);
    val=object["End Point"];
    arr=val.toObject();
    point.X=arr["X"].toDouble();
    point.Y=arr["Y"].toDouble();
    point.Z=arr["Z"].toDouble();
    move->set_end_point(point);
    point.X=arr["dX"].toDouble();
    point.Y=arr["dY"].toDouble();
    point.Z=arr["dZ"].toDouble();
    move->set_precision_end(point);
    string str=object["Move type"].toString().toStdString();
    if (str == "move to point")
    {
        move->set_move_type(pointMove);
    }
    else if (str == "line move")
    {
        move->set_move_type(lineMove);
    }
    str=object["Depth type"].toString().toStdString();
    if (str == "constant depth")
    {
        move->set_depth_type(constDepth);
    }
    else if (str == "constant hight")
    {
        move->set_depth_type(constHight);
    }
    else if (str == "any depth")
    {
        move->set_depth_type(anyDepth);
    }
    return move;
}

MyGals *MainWindow::scan_gals_from_json(QJsonObject object)
{
    MyGals* gals = new MyGals;
    QJsonValue val;
    QJsonObject arr;
    MyPoint point;
    gals->set_name(object["Name"].toString().toStdString());
    val=object["Start Point"];
    arr=val.toObject();
    point.X=arr["X"].toDouble();
    point.Y=arr["Y"].toDouble();
    point.Z=arr["Z"].toDouble();
    gals->set_start_point(point);
    point.X=arr["dX"].toDouble();
    point.Y=arr["dY"].toDouble();
    point.Z=arr["dZ"].toDouble();
    gals->set_precision_start(point);
    val=object["End Point"];
    arr=val.toObject();
    point.X=arr["X"].toDouble();
    point.Y=arr["Y"].toDouble();
    point.Z=arr["Z"].toDouble();
    gals->set_end_point(point);
    point.X=arr["dX"].toDouble();
    point.Y=arr["dY"].toDouble();
    point.Z=arr["dZ"].toDouble();
    gals->set_precision_end(point);
    gals->set_length();
    gals->set_hight(object["Hight"].toDouble());
    gals->set_pitch(object["Pitch"].toDouble());
    return gals;
}
