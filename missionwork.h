#ifndef MISSIONWORK_H
#define MISSIONWORK_H

#include "MissionClasses/mymission.h"
#include "MissionClasses/mydive.h"
#include "MissionClasses/mygals.h"
#include "MissionClasses/mylift.h"
#include "MissionClasses/mymove.h"
#include "MissionClasses/myreturn.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QFile>
#include <QDebug> // если отключу, обязательно найдется еще ошибка и мне придется опять подключить, чтобы ее найти

#include <vector>

enum MissionType {typeDive, typeLift, typeMove, typeGals, typeReturn};

class MissionWork
{
private:

    vector <MyMission*> thisMission;

public:

    MissionWork();

    void add_mission(MyMission *m);
    void exchange_mission(int n1, int n2);
    void delete_mission(int n);

    void print_mission_to_json(string jsonName, string name);
    void print_mission_to_txt(string txtName, string name);
    string print_mission_to_show();
    //string scan_mission_from_json(string jsonName);
    void scan_mission_from_txt(string txtName);

    /*MyDive* scan_dive_from_json(QJsonObject object); // здесь создавались объекты типов миссий,
    MyLift* scan_lift_from_json(QJsonObject object);   // но так как они создавались внутри функций,
    MyMove* scan_move_from_json(QJsonObject object);   // то сразу после выхода из них уничтожались,
    MyGals* scan_gals_from_json(QJsonObject object);*/ // поэтому пришлось реализовать чтение полностью в mainwindow

    MissionType get_mission_type(MyMission* m);
    void clear_mission();
    int get_size();
    MyPoint get_start_point_mission(int n);
    MyPoint get_end_point_mission(int n);
    MyMission* get_mission(int n);
};

#endif // MISSIONWORK_H
