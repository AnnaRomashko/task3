#include "missionwork.h"

MissionWork::MissionWork()
{

}

MissionType MissionWork::get_mission_type(MyMission *m)
{
    if (dynamic_cast<MyDive*>(m))
    {
        return typeDive;
    }
    else if (dynamic_cast<MyLift*>(m))
    {
        return typeLift;
    }
    else if (dynamic_cast<MyMove*>(m))
    {
        return typeMove;
    }
    else if (dynamic_cast<MyGals*>(m))
    {
        return typeGals;
    }
    else if (dynamic_cast<MyReturn*>(m))
    {
        return typeReturn;
    }
}

void MissionWork::clear_mission()
{
    for (int i=0;i<this->thisMission.size();i++)
    {
        this->thisMission[i]->~MyMission();
    }
    this->thisMission.clear();
}

int MissionWork::get_size()
{
    return this->thisMission.size();
}

void MissionWork::add_mission(MyMission *m)
{
    this->thisMission.push_back(m);
}

MyPoint MissionWork::get_start_point_mission(int n)
{
    return this->thisMission[n]->get_start_point();
}

MyPoint MissionWork::get_end_point_mission(int n)
{
    return this->thisMission[n]->get_end_point();
}

MyMission *MissionWork::get_mission(int n)
{
    return this->thisMission[n];
}

void MissionWork::delete_mission(int n)
{
    if (n+1 == this->thisMission.size())
    {
        this->thisMission.pop_back();
        return;
    }
    MyPoint start=this->thisMission[n]->get_start_point();
    for (int i=n; i<this->thisMission.size()-1;i++)
    {
       MyPoint s=this->thisMission[i+1]->get_start_point();
       MyPoint e=this->thisMission[i+1]->get_end_point();
       this->thisMission[i+1]->set_start_point(start);
       this->thisMission[i+1]->set_end_point(start+(e-s));
       if (this->thisMission[i+1]->get_error() != "OK")            // при выходе за границы буду перемещать точку конца предыдущей и начала следующей миссии в начало координат
       {                                                           // тогда будет минимальное количество следующих миссий, которые могут выходить за границы
           MyPoint zero=this->thisMission[i+1]->get_start_point(); // глубину оставляю как в точке начала, чтобы не нарушать возможный тип движения с постоянной глубиной
           zero.X=0;
           zero.Z=0;
           this->thisMission[i+1]->set_end_point(zero);
       }
       start=this->thisMission[i+1]->get_end_point();
    }
    for (int i=n; i<this->thisMission.size()-1;i++)
    {
        this->thisMission[i]=this->thisMission[i+1];
    }
    this->thisMission.pop_back();
}

void MissionWork::exchange_mission(int n1, int n2)
{
    if (n1>n2)
    {
        int n=n1;
        n1=n2;
        n2=n;
    }
    MyMission* m=this->thisMission[n1];
    this->thisMission[n1]=this->thisMission[n2];
    this->thisMission[n2]=m;
    MyPoint start=this->thisMission[n1]->get_start_point();
    MyPoint end=this->thisMission[n1]->get_end_point();
    this->thisMission[n1]->set_start_point(this->thisMission[n2]->get_start_point());
    this->thisMission[n1]->set_end_point(this->thisMission[n2]->get_start_point()+(end-start));
    if (this->thisMission[n1]->get_error() != "OK")
    {
        MyPoint zero=this->thisMission[n1]->get_start_point();
        zero.X=0;
        zero.Z=0;
        this->thisMission[n1]->set_end_point(zero);
    }
    MyPoint l=end-start;
    start=this->thisMission[n1]->get_end_point();
    for (int i=n1+1;i<this->thisMission.size();i++)
    {
        MyPoint s=this->thisMission[i]->get_start_point();
        MyPoint e=this->thisMission[i]->get_end_point();
        this->thisMission[i]->set_start_point(start);
        end=start+(e-s);
        this->thisMission[i]->set_end_point(end);
        if (this->thisMission[i]->get_error() != "OK")
        {
            MyPoint zero=this->thisMission[i]->get_start_point();
            zero.X=0;
            zero.Z=0;
            this->thisMission[i]->set_end_point(zero);
        }
        start=this->thisMission[i]->get_end_point();
    }
}

void MissionWork::print_mission_to_json(string jsonName, string name)
{
    QFile jsonFile(QString::fromStdString(jsonName));
    jsonFile.open(QIODevice::WriteOnly);
    jsonFile.resize(0);
    QJsonDocument doc;
    QJsonObject mission;
    mission["Mission"]=QString::fromStdString(name);
    mission["Number of operations"]=(int)this->thisMission.size();
    for (int i=1;i<=this->thisMission.size();i++)
    {
        mission[QString::fromStdString("№ "+to_string(i))]=this->thisMission[i-1]->print_mission_to_json_object();
    }
    doc.setObject(mission);
    jsonFile.write(doc.toJson());
    jsonFile.close();
}

void MissionWork::print_mission_to_txt(string txtName, string name)
{
    QFile txtFile(QString::fromStdString(txtName));
    txtFile.open(QIODevice::WriteOnly);
    txtFile.resize(0);
    string str="Mission "+name+"\n";
    txtFile.write(str.c_str());
    str=print_mission_to_show();
    txtFile.write(str.c_str());
    txtFile.close();
}

string MissionWork::print_mission_to_show()
{
    string str;
    for (int i=0;i<this->thisMission.size();i++)
    {
        str=str+"№ " + to_string(i+1)+ " ";
        str=str+this->thisMission[i]->print_mission_to_string();
        str=str+"\n";
    }
    return str;
}


/*string MissionWork::scan_mission_from_json(string jsonName)
{
    QFile jsonFile(QString::fromStdString(jsonName));
    jsonFile.open(QIODevice::ReadOnly);
    QJsonDocument doc(QJsonDocument::fromJson(jsonFile.readAll()));
    QJsonObject mainObject=doc.object();
    QJsonValue val=mainObject["Mission"];
    string name=val.toString().toStdString();
    val=mainObject["Number of operations"];
    int n=val.toInt();
    this->clear_mission();
    QJsonObject mission;
    for (int i=1;i<n;i++)
    {
        val=mainObject[QString::fromStdString("№ "+to_string(i))];
        mission=val.toObject();
        val=mission["Type"];
        string typeStr=val.toString().toStdString();
        if (typeStr == "Dive")
        {
            this->thisMission.push_back(scan_dive_from_json(mission));
        }
        else if (typeStr == "Lift")
        {
            this->thisMission.push_back(scan_lift_from_json(mission));
        }
        else if (typeStr == "Move")
        {
            this->thisMission.push_back(scan_move_from_json(mission));
        }
        else if (typeStr == "Gals")
        {
            this->thisMission.push_back(scan_gals_from_json(mission));
        }
    }
    jsonFile.close();
    return name;
}*/

void MissionWork::scan_mission_from_txt(string txtName)
{
    // долго писать, json файла достаточно
}

/*MyDive *MissionWork::scan_dive_from_json(QJsonObject object)
{
    MyDive* dive= new MyDive;
    QJsonValue val;
    QJsonObject arr;
    MyPoint point;
    dive->set_name(object["Name"].toString().toStdString());
    val=object["Start Point"];
    arr=val.toObject();
    point.X=arr["X"].toDouble();
    point.Y=arr["Y"].toDouble();
    point.Z=arr["Z"].toDouble();
    dive->set_start_point(point);
    point.X=arr["dX"].toDouble();
    point.Y=arr["dY"].toDouble();
    point.Z=arr["dZ"].toDouble();
    dive->set_precision_start(point);
    val=object["End Point"];
    arr=val.toObject();
    point.X=arr["X"].toDouble();
    point.Y=arr["Y"].toDouble();
    point.Z=arr["Z"].toDouble();
    dive->set_end_point(point);
    point.X=arr["dX"].toDouble();
    point.Y=arr["dY"].toDouble();
    point.Z=arr["dZ"].toDouble();
    dive->set_precision_end(point);
    val=object["Depth"];
    arr=val.toObject();
    dive->set_depth(arr["H"].toDouble());
    dive->set_depth_precision(arr["dH"].toDouble());
    string str=object["Sensor type"].toString().toStdString();
    if (str == "depth sensor")
    {
        dive->set_sensor_type(depthSensor);
    }
    else if (str == "sonar")
    {
        dive->set_sensor_type(sonarSensor);
    }
    str=object["Dive type"].toString().toStdString();
    if (str == "spiral")
    {
        dive->set_dive_type(spiralDive);
        double r=object["Spiral radius"].toDouble();
        double p=object["Spiral pitch"].toDouble();
        Spiral sp(dive->get_start_point(),dive->get_end_point(),r,p);
        dive->set_spiral_param(sp);
    }
    else if (str == "line")
    {
        dive->set_dive_type(lineDive);
    }
    else if (str == "vertical")
    {
        dive->set_dive_type(verticalDive);
    }
    return dive;
}

MyLift *MissionWork::scan_lift_from_json(QJsonObject object)
{
    MyLift* lift = new MyLift;
    QJsonValue val;
    QJsonObject arr;
    MyPoint point;
    lift->set_name(object["Name"].toString().toStdString());
    val=object["Start Point"];
    arr=val.toObject();
    point.X=arr["X"].toDouble();
    point.Y=arr["Y"].toDouble();
    point.Z=arr["Z"].toDouble();
    lift->set_start_point(point);
    point.X=arr["dX"].toDouble();
    point.Y=arr["dY"].toDouble();
    point.Z=arr["dZ"].toDouble();
    lift->set_precision_start(point);
    val=object["End Point"];
    arr=val.toObject();
    point.X=arr["X"].toDouble();
    point.Y=arr["Y"].toDouble();
    point.Z=arr["Z"].toDouble();
    lift->set_end_point(point);
    point.X=arr["dX"].toDouble();
    point.Y=arr["dY"].toDouble();
    point.Z=arr["dZ"].toDouble();
    lift->set_precision_end(point);
    val=object["Depth"];
    arr=val.toObject();
    lift->set_depth(arr["H"].toDouble());
    lift->set_depth_precision(arr["dH"].toDouble());
    string str=object["Sensor type"].toString().toStdString();
    if (str == "depth sensor")
    {
        lift->set_sensor_type(depthSensor);
    }
    else if (str == "sonar")
    {
        lift->set_sensor_type(sonarSensor);
    }
    str=object["Dive type"].toString().toStdString();
    if (str == "spiral")
    {
        lift->set_dive_type(spiralDive);
        double r=object["Spiral radius"].toDouble();
        double p=object["Spiral pitch"].toDouble();
        Spiral sp(lift->get_start_point(),lift->get_end_point(),r,p);
        lift->set_spiral_param(sp);
    }
    else if (str == "line")
    {
        lift->set_dive_type(lineDive);
    }
    else if (str == "vertical")
    {
        lift->set_dive_type(verticalDive);
    }
    return lift;
}

MyMove *MissionWork::scan_move_from_json(QJsonObject object)
{
    MyMove* move = new MyMove;
    QJsonValue val;
    QJsonObject arr;
    MyPoint point;
    move->set_name(object["Name"].toString().toStdString());
    val=object["Start Point"];
    arr=val.toObject();
    point.X=arr["X"].toDouble();
    point.Y=arr["Y"].toDouble();
    point.Z=arr["Z"].toDouble();
    move->set_start_point(point);
    point.X=arr["dX"].toDouble();
    point.Y=arr["dY"].toDouble();
    point.Z=arr["dZ"].toDouble();
    move->set_precision_start(point);
    val=object["End Point"];
    arr=val.toObject();
    point.X=arr["X"].toDouble();
    point.Y=arr["Y"].toDouble();
    point.Z=arr["Z"].toDouble();
    move->set_end_point(point);
    point.X=arr["dX"].toDouble();
    point.Y=arr["dY"].toDouble();
    point.Z=arr["dZ"].toDouble();
    move->set_precision_end(point);
    string str=object["Move type"].toString().toStdString();
    if (str == "move to point")
    {
        move->set_move_type(pointMove);
    }
    else if (str == "line move")
    {
        move->set_move_type(lineMove);
    }
    str=object["Depth type"].toString().toStdString();
    if (str == "constant depth")
    {
        move->set_depth_type(constDepth);
    }
    else if (str == "constant hight")
    {
        move->set_depth_type(constHight);
    }
    else if (str == "any depth")
    {
        move->set_depth_type(anyDepth);
    }
    return move;
}

MyGals *MissionWork::scan_gals_from_json(QJsonObject object)
{
    MyGals* gals = new MyGals;
    QJsonValue val;
    QJsonObject arr;
    MyPoint point;
    gals->set_name(object["Name"].toString().toStdString());
    val=object["Start Point"];
    arr=val.toObject();
    point.X=arr["X"].toDouble();
    point.Y=arr["Y"].toDouble();
    point.Z=arr["Z"].toDouble();
    gals->set_start_point(point);
    point.X=arr["dX"].toDouble();
    point.Y=arr["dY"].toDouble();
    point.Z=arr["dZ"].toDouble();
    gals->set_precision_start(point);
    val=object["End Point"];
    arr=val.toObject();
    point.X=arr["X"].toDouble();
    point.Y=arr["Y"].toDouble();
    point.Z=arr["Z"].toDouble();
    gals->set_end_point(point);
    point.X=arr["dX"].toDouble();
    point.Y=arr["dY"].toDouble();
    point.Z=arr["dZ"].toDouble();
    gals->set_precision_end(point);
    gals->set_length();
    gals->set_hight(object["Hight"].toDouble());
    gals->set_pitch(object["Pitch"].toDouble());
}*/
