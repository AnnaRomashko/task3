#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <QDoubleValidator>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>

#include <string>

#include "missionwork.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

enum Act {actStart, actCreate, actWait, actModify}; // выбранный пункт меню
enum WhatToDo {doStart, doDive, doLift, doMove, doGals, doChangeStart, doChangeEnd, doDelete, doExchange}; // нажатая кнопка

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void hide_mission_menu(); // спрятать кнопки
    void hide_mission_buttons();
    void hide_change();
    void hide_exchange();
    void hide_delete();

    void show_mission_menu();// показать кнопки
    void show_mission_buttons();
    void show_change(int count);
    void show_exchange(int count);
    void show_delete(int count);

    void update_mission_show();//обновить вывод миссии на экран

    void create_selected();//действия при выборе пунктов меню
    void open_selected();
    void save_selected();
    void close_selected();

    void dive_selected();//действия при нажатии ок после нажатия кнопок типов миссий
    void lift_selected();
    void move_selected();
    void gals_selected();

    void show_selected_mission();//действия при нажатии ок после нажатия кнопок действий с миссиями
    void change_selected();
    void exchange_selected();
    void delete_selected();

    string scan_mission_from_json(string jsonName); // чтение миссии из файла

    MyDive* scan_dive_from_json(QJsonObject object); // реалзованы здаесь, так как нужно
    MyLift* scan_lift_from_json(QJsonObject object); // создавать объекты типов миссий
    MyMove* scan_move_from_json(QJsonObject object); // (см. missionwork.h)
    MyGals* scan_gals_from_json(QJsonObject object);

private slots:

    void on_pushButtonDive_clicked();//действия при нажатии кнопок типов миссий
    void on_pushButtonLift_clicked();
    void on_pushButtonMove_clicked();
    void on_pushButtonGals_clicked();

    void on_pushButtonChange_clicked();// действия при нажатии кнопок действий с миссиями
    void on_pushButtonExchange_clicked();
    void on_pushButtonDelete_clicked();

    void on_pushButtonOk_clicked();//нажатие кнопки ок

private:

    Act ActNow; // выбранный пункт меню
    WhatToDo ToDoNow; // выбранное действие из кнопок

    const double precDefault = 0.5; // допуск по умолчанию
    std::string thisName; // название миссии
    WhatToDo currentMissionType; // используется для запоминание типа миссии, которую изменяют по кнопке change

    Ui::MainWindow *ui;
    MissionWork missionNow; // хранение миссии
};
#endif // MAINWINDOW_H
