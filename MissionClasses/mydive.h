#ifndef MYDIVE_H
#define MYDIVE_H
#include "mymission.h"
#include <math.h>

class MyDive : public MyMission
{

protected:

    SensorType sensorType; // тип датчика глубины
    double depth; // глубина погружение
    DiveType diveType; // тип погружения
    double depthPrecision; // погрешность задания глубины погружения
    Spiral spiraldive; // используются в зависимости от выбранного типа погружения

public:

    MyDive();
    virtual ~MyDive();

    void set_sensor_type(SensorType sensor);
    void set_depth(double d);
    void set_dive_type(DiveType dive);
    void set_name(string name);
    void set_depth_precision(double precision);
    void set_spiral_param(Spiral spiral);

    SensorType get_sensor_type();
    double get_depth();
    DiveType get_dive_type();
    double get_depth_precision();
    Spiral get_spiral_param();
    Line get_line_param();

    bool if_correct_spiral(Spiral spiral); // проверяет, что спираль не выходит за границы поля

    void calculate_depth();

    virtual void print_mission();
    virtual string print_mission_to_string();
    virtual QJsonObject print_mission_to_json_object();
};

#endif // MYDIVE_H
