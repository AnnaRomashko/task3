#include "mylift.h"

MyLift::MyLift()
{
    nameMission="Lift";
}

MyLift::~MyLift()
{

}

void MyLift::set_name(string name)
{
    this->errorMission="OK";
    if (name == "") nameMission="Lift";
    else nameMission=name;
}

void MyLift::set_depth(double d)
{
    if (this->depth<0)
    {
        errorMission="depth should be absolute value as it is length, not direction";
        return;
    }
    MyPoint p=this->startPoint;
    p.Y=p.Y-d;
    if (!if_correct_point(p))
    {
        errorMission="depth is out of workplace";
        return;
    }
    this->errorMission="OK";
    this->depth=d;
    this->endPoint=p;
}

void MyLift::print_mission()
{
    if (this->errorMission != "OK")
    {
        cout << "Error! " << this->errorMission;
        return;
    }
    cout << "Lift " << this->nameMission << endl;
    cout << "Start Point: x=" << this->startPoint.X << "+/-" << this->precisionStart.X << " y="<<this->startPoint.Y << "+/-" << this->precisionStart.Y << " z="<<this->startPoint.Z << "+/-" << this->precisionStart.Z << endl;
    cout << "End Point: x=" << this->endPoint.X << "+/-" << this->precisionEnd.X << " y="<<this->endPoint.Y << "+/-" << this->precisionEnd.Y << " z="<<this->endPoint.Z << "+/-" << this->precisionEnd.Z << endl;
    cout << "Depth = " << this->get_depth() << "+/-" << this->get_depth_precision()<< endl;
    cout << "Sensor type: ";
    switch (this->get_sensor_type())
    {
    case depthSensor:
    {
    cout << "depth sensor" << endl;
    break;
    }
    case sonarSensor:
    {
    cout << "sonar" << endl;
    break;
    }
    }
    cout << "Lift type: ";
    switch(this->get_dive_type())
    {
    case spiralDive:
    {
        cout << "spiral with parametres:" << endl;
        Spiral s=this->get_spiral_param();
        cout << "radius= " << s.radiusSpiral << endl;
        cout << "pitch=" << s.pitchSpiral << endl;
        break;
    }
    case lineDive:
    {
        cout << "line" << endl;
        break;
    }
    case verticalDive:
    {
        cout << "vertical" << endl;
        break;
    }
    }
}

string MyLift::print_mission_to_string()
{
    string str="";
    if (this->errorMission != "OK")
    {
        str= "Error! " + this->errorMission;
        return str;
    }
    str = "Lift " + this->nameMission + "\n";
    str=str+"Start Point: x=" + to_string(this->startPoint.X) + "+/-" + to_string(this->precisionStart.X) + " y=" + to_string(this->startPoint.Y) + "+/-" + to_string(this->precisionStart.Y) + " z=" + to_string(this->startPoint.Z) + "+/-" + to_string(this->precisionStart.Z) + "\n";
    str=str+"End Point: x=" + to_string(this->endPoint.X) + "+/-" + to_string(this->precisionEnd.X) + " y=" + to_string(this->endPoint.Y) + "+/-" + to_string(this->precisionEnd.Y) + " z=" + to_string(this->endPoint.Z) + "+/-" + to_string(this->precisionEnd.Z) + "\n";
    str=str+"Depth = " + to_string(this->get_depth()) + "+/-" + to_string(this->get_depth_precision()) + "\n";
    str=str+"Sensor type: ";
    switch (this->get_sensor_type())
    {
    case depthSensor:
    {
    str=str+ "depth sensor" + "\n";
    break;
    }
    case sonarSensor:
    {
    str=str+ "sonar" + "\n";
    break;
    }
    }
    str=str+"Lift type: ";
    switch(this->get_dive_type())
    {
    case spiralDive:
    {
        str=str+ "spiral with parametres:" + "\n";
        Spiral s=this->get_spiral_param();
        str=str+ "radius= " + to_string(s.radiusSpiral) + "\n";
        str=str+ "pitch=" + to_string(s.pitchSpiral) + "\n";
        break;
    }
    case lineDive:
    {
        str=str+ "line" + "\n";
        break;
    }
    case verticalDive:
    {
        str=str+ "vertical" + "\n";
        break;
    }
    }
    return str;
}

QJsonObject MyLift::print_mission_to_json_object()
{
    QJsonObject object;
    object["Type"] = "Lift";
    object["Name"] = QString::fromStdString(this->nameMission);
    QJsonObject startarray;
    startarray["X"]=this->startPoint.X;
    startarray["dX"]= this->precisionStart.X;
    startarray["Y"]=this->startPoint.Y;
    startarray["dY"]= this->precisionStart.Y;
    startarray["Z"]=this->startPoint.Z;
    startarray["dZ"]= this->precisionStart.Z;
    object["Start Point"] = startarray;
    QJsonObject endarray;
    endarray["X"]=this->endPoint.X;
    endarray["dX"]= this->precisionEnd.X;
    endarray["Y"]=this->endPoint.Y;
    endarray["dY"]= this->precisionEnd.Y;
    endarray["Z"]=this->endPoint.Z;
    endarray["dZ"]= this->precisionEnd.Z;
    object["End Point"] = endarray;
    QJsonObject deptharray;
    deptharray["H"]=this->get_depth();
    deptharray["dH"]= this->get_depth_precision();
    object["Depth"] = deptharray;
    switch (this->get_sensor_type())
    {
    case depthSensor:
    {
    object["Sensor type"] = "depth sensor";
    break;
    }
    case sonarSensor:
    {
    object["Sensor type"] = "sonar";
    break;
    }
    }
    switch(this->get_dive_type())
    {
    case spiralDive:
    {
        object["Lift type"] = "spiral";
        Spiral s=this->get_spiral_param();
        object["Spiral radius"] = s.radiusSpiral;
        object["Spiral pitch"] = s.pitchSpiral;
        break;
    }
    case lineDive:
    {
        object["Lift type"] = "line";
        break;
    }
    case verticalDive:
    {
        object["Lift type"] = "vertical";
        break;
    }
    }
    return object;
}
