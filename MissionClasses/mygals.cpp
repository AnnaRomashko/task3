#include "mygals.h"

MyGals::MyGals()
{
    pitchGals=0;
    hightGals=0;
    lengthGals=0;
    nameMission="Gals";
}

MyGals::~MyGals()
{

}

void MyGals::calculate_length()
{
    Line l(this->startPoint,this->endPoint);
    this->lengthGals=l.lengthLine;
}

void MyGals::set_pitch(double p)
{
    if (p<0)
    {
        errorMission="pitch cant be less then zero";
        return;
    }
    errorMission="OK";
    this->pitchGals=p;
}

void MyGals::set_hight(double h)
{
    if (h<0)
    {
        errorMission="hight cant be less then zero";
        return;
    }
    errorMission="OK";
    this->hightGals=h;
    calculate_length();
}

void MyGals::set_length()
{
    this->errorMission="OK";
    this->calculate_length();
}

double MyGals::get_pitch()
{
    return this->pitchGals;
}

double MyGals::get_hight()
{
    return this->hightGals;
}

double MyGals::get_length()
{
    return this->lengthGals;
}

void MyGals::print_mission()
{
    if (this->errorMission != "OK")
    {
        cout << "Error! " << this->errorMission;
        return;
    }
    cout << "Gals " << this->nameMission << endl;
    cout << "Start Point: x=" << this->startPoint.X << "+/-" << this->precisionStart.X << " y="<<this->startPoint.Y << "+/-" << this->precisionStart.Y << " z="<<this->startPoint.Z << "+/-" << this->precisionStart.Z << endl;
    cout << "End Point: x=" << this->endPoint.X << "+/-" << this->precisionEnd.X << " y="<<this->endPoint.Y << "+/-" << this->precisionEnd.Y << " z="<<this->endPoint.Z << "+/-" << this->precisionEnd.Z << endl;
    cout << "Pitch = " << this->pitchGals << endl;
    cout << "Length = " << this->lengthGals << endl;
    cout << "Hight = " << this->hightGals << endl;
}

string MyGals::print_mission_to_string()
{
    string str="";
    if (this->errorMission != "OK")
    {
        str= "Error! " + this->errorMission;
        return str;
    }
    str = "Gals " + this->nameMission + "\n";
    str=str+"Start Point: x=" + to_string(this->startPoint.X) + "+/-" + to_string(this->precisionStart.X) + " y=" + to_string(this->startPoint.Y) + "+/-" + to_string(this->precisionStart.Y) + " z=" + to_string(this->startPoint.Z) + "+/-" + to_string(this->precisionStart.Z) + "\n";
    str=str+"End Point: x=" + to_string(this->endPoint.X) + "+/-" + to_string(this->precisionEnd.X) + " y=" + to_string(this->endPoint.Y) + "+/-" + to_string(this->precisionEnd.Y) + " z=" + to_string(this->endPoint.Z) + "+/-" + to_string(this->precisionEnd.Z) + "\n";
    str=str+"Pitch = " + to_string(this->pitchGals) + "\n";
    str=str+"Length = " + to_string(this->lengthGals) + "\n";
    str=str+"Hight = " + to_string(this->hightGals) + "\n";
    return str;
}

QJsonObject MyGals::print_mission_to_json_object()
{
    QJsonObject object;
    object["Type"] = "Gals";
    object["Name"] = QString::fromStdString(this->nameMission);
    QJsonObject startarray;
    startarray["X"]=this->startPoint.X;
    startarray["dX"]= this->precisionStart.X;
    startarray["Y"]=this->startPoint.Y;
    startarray["dY"]= this->precisionStart.Y;
    startarray["Z"]=this->startPoint.Z;
    startarray["dZ"]= this->precisionStart.Z;
    object["Start Point"] = startarray;
    QJsonObject endarray;
    endarray["X"]=this->endPoint.X;
    endarray["dX"]= this->precisionEnd.X;
    endarray["Y"]=this->endPoint.Y;
    endarray["dY"]= this->precisionEnd.Y;
    endarray["Z"]=this->endPoint.Z;
    endarray["dZ"]= this->precisionEnd.Z;
    object["End Point"] = endarray;
    object["Pitch"] = this->pitchGals;
    object["Length"] = this->lengthGals;
    object["Hight"] = this->hightGals;
    return object;
}
