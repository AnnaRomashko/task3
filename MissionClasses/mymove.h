#ifndef MYMOVE_H
#define MYMOVE_H
#include "mymission.h"

class MyMove: public MyMission
{

private:

    MoveType moveType;
    DepthType depthType;
    Line Moveline;

public:

    MyMove();
    virtual ~MyMove();

    void set_name(string name);
    void set_move_type(MoveType m);
    void set_depth_type(DepthType d);
    void set_line_param(Line l);

    MoveType get_move_type();
    DepthType get_depth_type();
    Line get_line_param();

    virtual void print_mission();
    virtual string print_mission_to_string();
    virtual QJsonObject print_mission_to_json_object();
};

#endif // MYMOVE_H
