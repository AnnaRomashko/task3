#ifndef MYMISSION_H
#define MYMISSION_H
#include "helpclasses.h"
#include <QJsonObject>
#include <QString>
#include <iostream>

using namespace std;

// Те элементы, что используются в нескольких дочерних классах, но не во всех

// Для классов Dive и Lift
enum SensorType {depthSensor, sonarSensor}; // тип датчика глубины
enum DiveType {spiralDive, verticalDive, lineDive}; // тип погружения

// Для класса Move
enum MoveType {pointMove, lineMove}; // тип движения
enum DepthType {constDepth, constHight, anyDepth}; // тип поддержания глубины при движении

class MyMission // родительский класс миссий
{

protected:

    MyPoint startPoint; // координаты начальной точки
    MyPoint endPoint; // координаты конечной точки
    MyPoint precisionStart; // точность координат начальной точки
    MyPoint precisionEnd; // точность координат конечной точки
    string errorMission; // ошибки
    string nameMission; // название миссии

public:

    MyMission();
    MyMission(string name, MyPoint start, MyPoint end, MyPoint precisions, MyPoint precisione);
    virtual ~MyMission();

    void set_start_point(MyPoint point);
    void set_end_point(MyPoint point);
    void set_precision_start(MyPoint point);
    void set_precision_end(MyPoint point);
    void set_name(string name);

    MyPoint get_start_point();
    MyPoint get_end_point();
    MyPoint get_precision_start();
    MyPoint get_precision_end();
    string get_name();
    string get_error();

    bool if_correct_point(MyPoint point); // проверяет, что точка находится внутри поля
    bool if_correct_line(Line line); // проверяет, что линия не выходит за границы поля

    virtual void print_mission();
    virtual string print_mission_to_string();
    virtual QJsonObject print_mission_to_json_object();
};

#endif // MYMISSION_H
