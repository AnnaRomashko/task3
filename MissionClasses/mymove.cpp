#include "mymove.h"

MyMove::MyMove()
{
    moveType=pointMove;
    depthType=constDepth;
    nameMission="Move";
    Line l0;
    Moveline=l0;
}

MyMove::~MyMove()
{

}

void MyMove::set_name(string name)
{
    this->errorMission="OK";
    if (name == "") nameMission="Move";
    else nameMission=name;
}

void MyMove::set_move_type(MoveType m)
{
    this->errorMission="OK";
    this->moveType=m;
}

void MyMove::set_depth_type(DepthType d)
{
    this->errorMission="OK";
    this->depthType=d;
}

void MyMove::set_line_param(Line l)
{
    this->errorMission="OK";
    if (!if_correct_line(l))
    {
        this->errorMission="line is out of workplace";
        return;
    }
    this->Moveline=l;
}

MoveType MyMove::get_move_type()
{
    return this->moveType;
}

DepthType MyMove::get_depth_type()
{
    return this->depthType;
}

Line MyMove::get_line_param()
{
    return this->Moveline;
}

void MyMove::print_mission()
{
    if (this->errorMission != "OK")
    {
        cout << "Error! " << this->errorMission;
        return;
    }
    cout << "Move " << this->nameMission << endl;
    cout << "Start Point: x=" << this->startPoint.X << "+/-" << this->precisionStart.X << " y="<<this->startPoint.Y << "+/-" << this->precisionStart.Y << " z="<<this->startPoint.Z << "+/-" << this->precisionStart.Z << endl;
    cout << "End Point: x=" << this->endPoint.X << "+/-" << this->precisionEnd.X << " y="<<this->endPoint.Y << "+/-" << this->precisionEnd.Y << " z="<<this->endPoint.Z << "+/-" << this->precisionEnd.Z << endl;
    cout << "Move type: ";
    switch (this->moveType)
    {
    case pointMove:
    {
    cout << "move to point" << endl;
    break;
    }
    case lineMove:
    {
    cout << "line move" << endl;
    break;
    }
    }
    cout << "Depth type: ";
    switch(this->depthType)
    {
    case constDepth:
    {
        cout << "constant depth" << endl;
        break;
    }
    case constHight:
    {
        cout << "constant hight" << endl;
        break;
    }
    case anyDepth:
    {
        cout << "any depth" << endl;
        break;
    }
    }
}

string MyMove::print_mission_to_string()
{
    string str="";
    if (this->errorMission != "OK")
    {
        str= "Error! " + this->errorMission;
        return str;
    }
    str = "Move " + this->nameMission + "\n";
    str=str+"Start Point: x=" + to_string(this->startPoint.X) + "+/-" + to_string(this->precisionStart.X) + " y=" + to_string(this->startPoint.Y) + "+/-" + to_string(this->precisionStart.Y) + " z=" + to_string(this->startPoint.Z) + "+/-" + to_string(this->precisionStart.Z) + "\n";
    str=str+"End Point: x=" + to_string(this->endPoint.X) + "+/-" + to_string(this->precisionEnd.X) + " y=" + to_string(this->endPoint.Y) + "+/-" + to_string(this->precisionEnd.Y) + " z=" + to_string(this->endPoint.Z) + "+/-" + to_string(this->precisionEnd.Z) + "\n";
    str=str+"Move type: ";
    switch (this->moveType)
    {
    case pointMove:
    {
    str=str+ "move to point" + "\n";
    break;
    }
    case lineMove:
    {
    str=str+ "line move" + "\n";
    break;
    }
    }
    str=str+"Depth type: ";
    switch(this->depthType)
    {
    case constDepth:
    {
        str=str+ "constant depth" + "\n";
        break;
    }
    case constHight:
    {
        str=str+ "constant hight" + "\n";
        break;
    }
    case anyDepth:
    {
        str=str+ "any depth" + "\n";
        break;
    }
    }
    return str;
}

QJsonObject MyMove::print_mission_to_json_object()
{
    QJsonObject object;
    object["Type"] = "Move";
    object["Name"] = QString::fromStdString(this->nameMission);
    QJsonObject startarray;
    startarray["X"]=this->startPoint.X;
    startarray["dX"]= this->precisionStart.X;
    startarray["Y"]=this->startPoint.Y;
    startarray["dY"]= this->precisionStart.Y;
    startarray["Z"]=this->startPoint.Z;
    startarray["dZ"]= this->precisionStart.Z;
    object["Start Point"] = startarray;
    QJsonObject endarray;
    endarray["X"]=this->endPoint.X;
    endarray["dX"]= this->precisionEnd.X;
    endarray["Y"]=this->endPoint.Y;
    endarray["dY"]= this->precisionEnd.Y;
    endarray["Z"]=this->endPoint.Z;
    endarray["dZ"]= this->precisionEnd.Z;
    object["End Point"] = endarray;
    switch (this->moveType)
    {
    case pointMove:
    {
    object["Move type"] = "move to point";
    break;
    }
    case lineMove:
    {
    object["Move type"] = "line move";
    break;
    }
    }
    switch(this->depthType)
    {
    case constDepth:
    {
        object["Depth type"] = "constant depth";
        break;
    }
    case constHight:
    {
        object["Depth type"] = "constant hight";
        break;
    }
    case anyDepth:
    {
        object["Depth type"] = "any depth";
        break;
    }
    }
    return object;
}
