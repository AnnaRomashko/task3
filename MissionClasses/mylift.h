#ifndef MYLIFT_H
#define MYLIFT_H
#include "mydive.h"

class MyLift: public MyDive
{

public:

    MyLift();
    virtual ~MyLift();

    void set_name(string name);
    void set_depth(double d);

    virtual void print_mission();
    virtual string print_mission_to_string();
    virtual QJsonObject print_mission_to_json_object();
};

#endif // MYLIFT_H
