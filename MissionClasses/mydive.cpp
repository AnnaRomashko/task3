#include "mydive.h"

MyDive::MyDive()
{
    nameMission="Dive";
    depth=0;
    depthPrecision=0;
    sensorType=depthSensor;
    diveType=verticalDive;
}

MyDive::~MyDive()
{

}

void MyDive::set_sensor_type(SensorType sensor)
{
    this->errorMission="OK";
    this->sensorType=sensor;
}

void MyDive::set_depth(double d)
{
    if (depth<0)
    {
        errorMission="depth should be absolute value as it is length, not direction";
        return;
    }
    MyPoint p=this->startPoint;
    p.Y=p.Y+d;
    if (!if_correct_point(p))
    {
        errorMission="depth is out of workplace";
        return;
    }
    this->errorMission="OK";
    this->depth=d;
    this->endPoint=p;
}

void MyDive::set_dive_type(DiveType dive)
{
    this->errorMission="OK";
    this->diveType=dive;
}

void MyDive::set_name(string name)
{
    this->errorMission="OK";
    if (name == "") nameMission="Dive";
    else nameMission=name;
}

void MyDive::set_depth_precision(double precision)
{
    if (precision<0)
    {
        this->errorMission="precission cant be less then zero";
        return;
    }
    this->errorMission="OK";
    this->depthPrecision=precision;
}

void MyDive::set_spiral_param(Spiral spiral)
{
    this->errorMission="OK";
    if (this->diveType != spiralDive)
    {
        this->errorMission="dive type is not spiral";
        return;
    }
    if (this->if_correct_spiral(spiral)) this->spiraldive=spiral;
    else this->errorMission="spiral is out of workplace";
}

void MyDive::calculate_depth()
{
    this->depth=abs(endPoint.Y-startPoint.Y);
}

SensorType MyDive::get_sensor_type()
{
    return this->sensorType;
}

double MyDive::get_depth()
{
    return this->depth;
}

DiveType MyDive::get_dive_type()
{
    return this->diveType;
}

double MyDive::get_depth_precision()
{
    return this->depthPrecision;
}

Spiral MyDive::get_spiral_param()
{
    return this->spiraldive;
}

bool MyDive::if_correct_spiral(Spiral spiral)
{
    Line line(spiral.startSpiral,spiral.endSpiral); // по хорошему нужно написать функцию для спирали и проверить, что она не выходит за границы поля
    if (!if_correct_line(line)) return false;
    else return true;
}

void MyDive::print_mission()
{
    if (this->errorMission != "OK")
    {
        cout << "Error! " << this->errorMission;
        return;
    }
    cout << "Dive " << this->nameMission << endl;
    cout << "Start Point: x=" << this->startPoint.X << "+/-" << this->precisionStart.X << " y="<<this->startPoint.Y << "+/-" << this->precisionStart.Y << " z="<<this->startPoint.Z << "+/-" << this->precisionStart.Z << endl;
    cout << "End Point: x=" << this->endPoint.X << "+/-" << this->precisionEnd.X << " y="<<this->endPoint.Y << "+/-" << this->precisionEnd.Y << " z="<<this->endPoint.Z << "+/-" << this->precisionEnd.Z << endl;
    cout << "Depth = " << this->depth << "+/-" << this->depthPrecision<< endl;
    cout << "Sensor type: ";
    switch (this->sensorType)
    {
    case depthSensor:
    {
    cout << "depth sensor" << endl;
    break;
    }
    case sonarSensor:
    {
    cout << "sonar" << endl;
    break;
    }
    }
    cout << "Dive type: ";
    switch(this->diveType)
    {
    case spiralDive:
    {
        cout << "spiral with parametres:" << endl;
        cout << "radius= " << this->spiraldive.radiusSpiral << endl;
        cout << "pitch=" << this->spiraldive.pitchSpiral << endl;
        break;
    }
    case lineDive:
    {
        cout << "line" << endl;
        break;
    }
    case verticalDive:
    {
        cout << "vertical" << endl;
        break;
    }
    }
}

string MyDive::print_mission_to_string()
{
    string str="";
    if (this->errorMission != "OK")
    {
        str= "Error! " + this->errorMission;
        return str;
    }
    str = "Dive " + this->nameMission + "\n";
    str=str+"Start Point: x=" + to_string(this->startPoint.X) + "+/-" + to_string(this->precisionStart.X) + " y=" + to_string(this->startPoint.Y) + "+/-" + to_string(this->precisionStart.Y) + " z=" + to_string(this->startPoint.Z) + "+/-" + to_string(this->precisionStart.Z) + "\n";
    str=str+"End Point: x=" + to_string(this->endPoint.X) + "+/-" + to_string(this->precisionEnd.X) + " y=" + to_string(this->endPoint.Y) + "+/-" + to_string(this->precisionEnd.Y) + " z=" + to_string(this->endPoint.Z) + "+/-" + to_string(this->precisionEnd.Z) + "\n";
    str=str+"Depth = " + to_string(this->depth) + "+/-" + to_string(this->depthPrecision) + "\n";
    str=str+"Sensor type: ";
    switch (this->sensorType)
    {
    case depthSensor:
    {
    str=str+ "depth sensor" + "\n";
    break;
    }
    case sonarSensor:
    {
    str=str+ "sonar" + "\n";
    break;
    }
    }
    str=str+"Dive type: ";
    switch(this->diveType)
    {
    case spiralDive:
    {
        str=str+ "spiral with parametres:" + "\n";
        str=str+ "radius= " + to_string(this->spiraldive.radiusSpiral) + "\n";
        str=str+ "pitch=" + to_string(this->spiraldive.pitchSpiral) + "\n";
        break;
    }
    case lineDive:
    {
        str=str+ "line" + "\n";
        break;
    }
    case verticalDive:
    {
        str=str+ "vertical" + "\n";
        break;
    }
    }
    return str;
}

QJsonObject MyDive::print_mission_to_json_object()
{
    QJsonObject object;
    object["Type"] = "Dive";
    object["Name"] = QString::fromStdString(this->nameMission);
    QJsonObject startarray;
    startarray["X"]=this->startPoint.X;
    startarray["dX"]= this->precisionStart.X;
    startarray["Y"]=this->startPoint.Y;
    startarray["dY"]= this->precisionStart.Y;
    startarray["Z"]=this->startPoint.Z;
    startarray["dZ"]= this->precisionStart.Z;
    object["Start Point"] = startarray;
    QJsonObject endarray;
    endarray["X"]=this->endPoint.X;
    endarray["dX"]= this->precisionEnd.X;
    endarray["Y"]=this->endPoint.Y;
    endarray["dY"]= this->precisionEnd.Y;
    endarray["Z"]=this->endPoint.Z;
    endarray["dZ"]= this->precisionEnd.Z;
    object["End Point"] = endarray;
    QJsonObject deptharray;
    deptharray["H"]=this->depth;
    deptharray["dH"]= this->depthPrecision;
    object["Depth"] = deptharray;
    switch (this->sensorType)
    {
    case depthSensor:
    {
    object["Sensor type"] = "depth sensor";
    break;
    }
    case sonarSensor:
    {
    object["Sensor type"] = "sonar";
    break;
    }
    }
    switch(this->diveType)
    {
    case spiralDive:
    {
        object["Dive type"] = "spiral";
        object["Spiral radius"] = this->spiraldive.radiusSpiral;
        object["Spiral pitch"] = this->spiraldive.pitchSpiral;
        break;
    }
    case lineDive:
    {
        object["Dive type"] = "line";
        break;
    }
    case verticalDive:
    {
        object["Dive type"] = "vertical";
        break;
    }
    }
    return object;
}
