#ifndef MYGALS_H
#define MYGALS_H
#include "mymission.h"

class MyGals: public MyMission
{

private:

    double pitchGals;
    double hightGals; // высота прямоугольника
    double lengthGals; // длина прямоугольника

    void calculate_length();

public:

    MyGals();
    virtual ~MyGals();

    void set_pitch(double p);
    void set_hight(double h);
    void set_length();

    double get_pitch();
    double get_hight();
    double get_length();

    bool if_correct_pitch(int p);

    virtual void print_mission();
    virtual string print_mission_to_string();
    virtual QJsonObject print_mission_to_json_object();
};

#endif // MYGALS_H
