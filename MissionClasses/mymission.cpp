#include "mymission.h"

MyMission::MyMission()
{
    MyPoint point;
    point.X=0;
    point.Y=0;
    point.Z=0;
    startPoint=point;
    endPoint=point;
    precisionStart=point;
    precisionEnd=point;
    nameMission="Mission";
    errorMission="OK";
}

MyMission::MyMission(string name, MyPoint start, MyPoint end, MyPoint precisions, MyPoint precisione)
{
    startPoint=start;
    endPoint=end;
    precisionStart=precisions;
    precisionEnd=precisione;
    if (name == "") nameMission="Mission";
    else nameMission=name;
    errorMission="OK";
    if (!if_correct_point(start)) errorMission="start point is out of workplace";
    if (!if_correct_point(end)) errorMission="end point is out of workplace";
    if (precisionStart.X<0 || precisionStart.Y <0 || precisionStart.Z < 0 || precisionEnd.X < 0 || precisionEnd.Y < 0 || precisionEnd.Z < 0) errorMission="precission cant be less then zero";
}

MyMission::~MyMission()
{

}

void MyMission::set_start_point(MyPoint point)
{
    this->errorMission="OK";
    if (this->if_correct_point(point)) this->startPoint=point;
    else this->errorMission="point is out of workplace";
}

void MyMission::set_end_point(MyPoint point)
{
    this->errorMission="OK";
    if (this->if_correct_point(point)) this->endPoint=point;
    else this->errorMission="point is out of workplace";
}

void MyMission::set_precision_start(MyPoint point)
{
    this->errorMission="OK";
    this->precisionStart=point;
    if (precisionStart.X<0 || precisionStart.Y <0 || precisionStart.Z < 0) errorMission="precission cant be less then zero";
}

void MyMission::set_precision_end(MyPoint point)
{
    this->errorMission="OK";
    this->precisionEnd=point;
    if (precisionEnd.X < 0 || precisionEnd.Y < 0 || precisionEnd.Z < 0) errorMission="precission cant be less then zero";
}

void MyMission::set_name(string name)
{
    this->errorMission="OK";
    if (name == "") nameMission="Mission";
    else nameMission=name;
}

MyPoint MyMission::get_start_point()
{
    return this->startPoint;
}

MyPoint MyMission::get_end_point()
{
    return this->endPoint;
}

MyPoint MyMission::get_precision_start()
{
    return this->precisionStart;
}

MyPoint MyMission::get_precision_end()
{
    return this->precisionEnd;
}

string MyMission::get_name()
{
    return this->nameMission;
}

string MyMission::get_error()
{
    return this->errorMission;
}

bool MyMission::if_correct_point(MyPoint point)
{
    if (point.X < 0 || point.X > 10000 || point.Y < 0 || point.Y > 100 || point.Z < 0 || point.Z > 10000)
        return false;
    else return true;
}

bool MyMission::if_correct_line(Line line)
{
    if (!if_correct_point(line.startLine) || !if_correct_point(line.endLine))
        return false;
    else return true;
}

void MyMission::print_mission()
{
    if (this->errorMission != "OK")
    {
        cout << "Error! " << this->errorMission;
        return;
    }
    cout << "Mission " << this->nameMission << endl;
}

string MyMission::print_mission_to_string()
{
    string str="";
    if (this->errorMission != "OK")
    {
        str= "Error! " + this->errorMission;
        return str;
    }
    str = "Mission " + this->nameMission + "\n";
    return str;
}

QJsonObject MyMission::print_mission_to_json_object()
{
    QJsonObject object;
    object["Type"]="Mission";
    object["Name"] = QString::fromStdString(this->nameMission);
    return object;
}
