#include "myreturn.h"

MyReturn::MyReturn()
{
    nameMission="Return";
    endPoint.Y=0;
}

MyReturn::~MyReturn()
{

}

void MyReturn::print_mission()
{
    if (this->errorMission != "OK")
    {
        cout << "Error! " << this->errorMission;
        return;
    }
    cout << "Return " << this->nameMission << endl;
    cout << "Start Point: x=" << this->startPoint.X << "+/-" << this->precisionStart.X << " y="<<this->startPoint.Y << "+/-" << this->precisionStart.Y << " z="<<this->startPoint.Z << "+/-" << this->precisionStart.Z << endl;
    cout << "End Point: x=" << this->endPoint.X << "+/-" << this->precisionEnd.X << " y="<<this->endPoint.Y << "+/-" << this->precisionEnd.Y << " z="<<this->endPoint.Z << "+/-" << this->precisionEnd.Z << endl;
}

string MyReturn::print_mission_to_string()
{
    string str="";
    if (this->errorMission != "OK")
    {
        str= "Error! " + this->errorMission;
        return str;
    }
    str = "Return " + this->nameMission + "\n";
    str=str+"Start Point: x=" + to_string(this->startPoint.X) + "+/-" + to_string(this->precisionStart.X) + " y=" + to_string(this->startPoint.Y) + "+/-" + to_string(this->precisionStart.Y) + " z=" + to_string(this->startPoint.Z) + "+/-" + to_string(this->precisionStart.Z) + "\n";
    str=str+"End Point: x=" + to_string(this->endPoint.X) + "+/-" + to_string(this->precisionEnd.X) + " y=" + to_string(this->endPoint.Y) + "+/-" + to_string(this->precisionEnd.Y) + " z=" + to_string(this->endPoint.Z) + "+/-" + to_string(this->precisionEnd.Z) + "\n";
    return str;
}

QJsonObject MyReturn::print_mission_to_json_object()
{
    QJsonObject object;
    object["Type"] = "Return";
    object["Name"] = QString::fromStdString(this->nameMission);
    QJsonObject startarray;
    startarray["X"]=this->startPoint.X;
    startarray["dX"]= this->precisionStart.X;
    startarray["Y"]=this->startPoint.Y;
    startarray["dY"]= this->precisionStart.Y;
    startarray["Z"]=this->startPoint.Z;
    startarray["dZ"]= this->precisionStart.Z;
    object["Start Point"] = startarray;
    QJsonObject endarray;
    endarray["X"]=this->endPoint.X;
    endarray["dX"]= this->precisionEnd.X;
    endarray["Y"]=this->endPoint.Y;
    endarray["dY"]= this->precisionEnd.Y;
    endarray["Z"]=this->endPoint.Z;
    endarray["dZ"]= this->precisionEnd.Z;
    object["End Point"] = endarray;
    return object;
}
