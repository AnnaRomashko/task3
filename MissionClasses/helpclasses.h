#ifndef HELPCLASSES_H
#define HELPCLASSES_H
#include <string>
#include <math.h>

using namespace std;

struct MyPoint // точка в трехмерном пространстве
{
    double X=0;
    double Y=0;
    double Z=0;

    MyPoint& operator=(const MyPoint& point)
    {
        this->X=point.X;
        this->Y=point.Y;
        this->Z=point.Z;
        return *this;
    }

    MyPoint operator+(const MyPoint& point)
    {
        MyPoint temp;
        temp.X=this->X+point.X;
        temp.Y=this->Y+point.Y;
        temp.Z=this->Z+point.Z;
        return temp;
    }

    MyPoint operator-(const MyPoint& point)
    {
        MyPoint temp;
        temp.X=this->X-point.X;
        temp.Y=this->Y-point.Y;
        temp.Z=this->Z-point.Z;
        return temp;
    }

    bool operator==(const MyPoint& point)
    {
        if (this->X == point.X && this->Y == point.Y && this->Z == point.Z)
            return true;
        else return false;
    }
};

struct Line
{
    MyPoint startLine;
    MyPoint endLine;
    MyPoint directionLine; //  нормированный вектор направления
    double lengthLine;
    string errorLine;

    void calculate_length()
    {
        this->lengthLine= sqrt(pow(this->startLine.X-this->endLine.X,2)+pow(this->startLine.Y-this->endLine.Y,2)+pow(this->startLine.Z-this->endLine.Z,2));
    }

    void calculate_direction()
    {
        this->directionLine=this->endLine-this->startLine;
        this->calculate_length();
        this->directionLine.X=this->directionLine.X/this->lengthLine;
        this->directionLine.Y=this->directionLine.Y/this->lengthLine;
        this->directionLine.Z=this->directionLine.Z/this->lengthLine;
    }

    Line()
    {
        MyPoint point;
        point.X=0;
        point.Y=0;
        point.Z=0;
        this->startLine=point;
        this->endLine=point;
        this->directionLine=point;
        this->lengthLine=0;
        this->errorLine="OK";
    }

    Line(MyPoint start, MyPoint end)
    {
        this->startLine=start;
        this->endLine=end;
        this->calculate_length();
        this->calculate_direction();
    }

    Line(MyPoint start, MyPoint direction, double length)
    {
        this->startLine=start;
        this->directionLine=direction;
        this->lengthLine=length;
        MyPoint dl;
        dl.X=this->directionLine.X*this->lengthLine;
        dl.Y=this->directionLine.Y*this->lengthLine;
        dl.Z=this->directionLine.Z*this->lengthLine;
    }

    bool is_line_correct()
    {
        this->errorLine="OK";
        if (this->lengthLine<0)
        {
            this->errorLine="incorrect line length";
            this->lengthLine=0;
            return false;
        }
        else return true;
    }
};

struct Spiral // спираль в трехмерном пространстве
{
    MyPoint startSpiral;
    MyPoint endSpiral;
    double radiusSpiral; // радиус окружносьт спирали
    double pitchSpiral; // шаг спирали
    double lengthSpiral; // длина, как расстояние между точками начала и конца
    MyPoint directionSpiral; //  нормированный вектор направления
    string errorSpiral;

    bool if_spiral_correct()
    {
        this->errorSpiral="OK";
        if (this->radiusSpiral < 0)
        {
            this->errorSpiral="incorrect spiral radius";
            this->radiusSpiral=0;
            return false;
        }
        else if (this->pitchSpiral < 0)
        {
            this->errorSpiral="incorrect spiral pitch";
            this->pitchSpiral=0;
            return false;
        }
        else if (this->lengthSpiral < 0)
        {
            this->errorSpiral="incorrect spiral length";
            this->lengthSpiral=0;
            return false;
        }
        else return true;
    }

    Spiral()
    {
        MyPoint point;
        point.X=0;
        point.Y=0;
        point.Z=0;
        this->startSpiral=point;
        this->endSpiral=point;
        this->radiusSpiral=0;
        this->pitchSpiral=0;
        this->lengthSpiral=0;
        this->errorSpiral="OK";
    }

    Spiral(MyPoint start, MyPoint end, double radius, double pitch)
    {
        this->startSpiral=start;
        this->endSpiral=end;
        this->radiusSpiral=radius;
        this->pitchSpiral=pitch;
        Line line(start, end);
        this->lengthSpiral=line.lengthLine;
        this->directionSpiral=line.directionLine;
        this->errorSpiral="OK";
        this->if_spiral_correct();
    }

    Spiral(MyPoint start, double radius, double pitch, double length, MyPoint direction)
    {
        this->startSpiral=start;
        Line line(start, direction, length);
        this->endSpiral=line.endLine;
        this->radiusSpiral=radius;
        this->pitchSpiral=pitch;
        this->lengthSpiral=length;
        this->directionSpiral=direction;
        this->errorSpiral="OK";
        this->if_spiral_correct();
    }
};

#endif // HELPCLASSES_H
