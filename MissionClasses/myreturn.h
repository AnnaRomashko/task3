#ifndef MYRETURN_H
#define MYRETURN_H
#include "mymission.h"

class MyReturn: public MyMission
{
public:

    MyReturn();
    virtual ~MyReturn();

    virtual void print_mission();
    virtual string print_mission_to_string();
    virtual QJsonObject print_mission_to_json_object();
};

#endif // MYRETURN_H
